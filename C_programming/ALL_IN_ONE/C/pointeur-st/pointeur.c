

UNIQUEMENT POINTEUR 


Notion fondamentale en C






                                     x=1234
valeur gauche (adresse de x)			valeur droite (contenu)

ptr_x = &x				|		*ptr_x   >   1234



NB :  terminologie  correcte

*ptr_x  est une  expresion d'indirection de pointeur  

 et ptr_x est le pointeur  ( la variable pointeur).




========================================================/


NB: >  fait référence

--------------------------------
L'opérateur d'indirection *

l'asterixque    *  porte le nom d'operateurd'indirection( ou d'adressage indirecte) lorsqu'il est utilisé comme comme operateur unaire.

	Il est possible de référencer la valeur d'une variable  en combinant l'operateur * et 'l'operande ( l'élément) contenant  l'adresse de la variable  

Ainsi: 

- avec :  char   x, *ptr_x   ;
 
l' expresion *ptr_x   >  la valeur de la variable x affectée à   au pointeur ptr_x
Ainsi, 

si x  = 1234, 
on peut  
	- affecter la valeur gauche (adresse)  de x     à      ptr_x
sous forme : ptr_x = &x ;


	Donc  l'expression *ptr_x  fera référence  à  1234  correspondant à la valeur droite (contenu)  de x 
	
	

RESUME:

					x=1234
valeur gauche (adresse de x)				valeur droite (contenu)

	ptr_x = &x			|		*ptr_x   >   1234
	
	

NB:  Pour clarifier la terminologie


   *ptr_x  est l' exprebsion d'indirection de pointeur : 

        et ptr_x est le pointeur  ( la variable pointeur). 




##############################################################"
##############################################################"


Autre Ex d'application: 

------------------------

Pour créer un objet  monObjet : 
de déclarer un pointeur sur le type FILE 


>    FILE* monObjet


-----------------------------------------------/






