/*   kce***    */
/*
From 
Imhotep-Lab

vieirasalva@gmail.com
 

*/


/*
A-O-zh.c  : all in one Zhang
*/


/*
Thanks to my students  Kouakou Nguessan Franck  and Mara Demba Etienne  for their  concentration and their obedience with the directives of the professeur.thanks to them, this works has been be carried out.That was  a great teaching exercise which must inspire the future teachers of Imhotep- Lab as regards of methology, rigour and excellence in the formation.  


                                                The professor

                                                Sanébankh THOUTMOS alias Ra777
                                                
*/

/*
The staff on Zhang's  book   for electronic version

        Rewritting: 

Kouakou Nguessan Franck 
 
Mara Demba Etienne 

         Supervision, comments and notes:
         



*/


/*   This is a coding training based on the Zhang's  book:
Tony Zhang,2002, Le Langage C, trad. ADAM Laurent,Campus Press, Paris.

Original Edition:   

Tony Zhang,1997, Teach Yourself C in 24 Hours,Sams.net Publishing, Indiana, Indianapolis.




 */

Contents  codes:

# ls03L02.c   07L01.c   09_L03.c  12L03.c   16L01.c  18L05.c  21L04.c
04_L01.c  07L02.c   09L04.c   12L04.c   16L02.c  19L01.c  22L01.c
04_L02.c  07L03.c   09L05.c   12L05.c   16L03.c  19L02.c  22L02.c
04_L03.c  07L04.c   10L01.c   12L06.c   16L04.c  19L03.c  22_L03.c
04L04.c   07L05.c   10L02.c   12L07.c   16L05.c  19L04.c  22L04.c
05L01.c   07L06.c   10L03.c   13L01.c   16L06.c  19L05.c  23L01.c
05L02..c  07L07.c   10L04.c   13L02.c   16L07.c  19L06.c  23L02.c
05L03.c   08_L01.c  10L05.c   13_L03.c  16L08.c  19L07.c  23L03.c
05L04.c   08_L02.c  10L06.c   13_L04.c  17L01.c  20L01.c  23L04.c
05L05.c   08_L03.c  10L07.c   13L05.c   17L02.c  20L02.c  outhaiku.txt
05L06.c   08L04.c   11L01.c   14L01.c   17L03.c  20L03.c  strnum.mix
05L07.c   08L05.c   11L02.c   14L02.c   17L04.c  20L04.c
05L08.c   08L06.c   11L03.c   14L03.c   18L01.c  20L05.c
06L02.c   08L07.c   11L04.c   15L01.c   18L02.c  21L01.c
06L03.c   09L01.c   12L01.c   15L02.c   18L03.c  21L02.c
06L04.c   09L02.c   12L02.c   15L03.c   18L04.c  21L03.c




============================================================/
/*
From Zhang 


*/


﻿
/*
pp 36-35

*/

#include <stdio.h>

// addition de 2 entier etrenvoi du resultat


int integer_add(int x, int y)
{
int result;
result= x+y;
 return result;
}

//Noter que la fct integer_add  est placée avant le main
int main(){
	int sum;
	sum=integer_add(5, 12);
	printf("5+12= donne %d.\n", sum);
	return 0;
	}


#############################################################################
#############################################################################

﻿/*
04L04.c    , p50

*/



#include <stdio.h>

main(){

	int int_num1, int_num2, int_num3;     // declaration de variable entieres


	float flt_num1, flt_num2, flt_num3;    // declaration de variable virgule flotante

	int_num1=32/10;                // diviseur et dividendde entier
	flt_num1=32/10;
	int_num2=32.0/10;              // diviseur  entier
	flt_num2=32.0/10;
	int_num3=32/10.0;               //  dividendde entier
	flt_num3=32/10.0;

	printf("la division entiere de 32 par 10=%d.\n", int_num1);
	printf("la division decimale de 32 par 10=%f.\n", flt_num1);
	printf("la division entiere de32.0par 10=%d.\n", int_num2);
	printf("la division decimale de 32.0 par 10=%f.\n", flt_num2);
	printf("la division entiere de32.0par 10.0=%d.\n", int_num3);
	printf("la division decimale de 32 par 10.0=%f.\n", flt_num3);

	return 0;
	}



#############################################################################
#############################################################################


/*

*/

/*
﻿lecture et écriture des E-S standards   
*/


/* prog qui lit le caractère entré au clavier ar l'utilisateur et l' affiche à l'ecran.



*/
// 05L01.c ....p59

#include <stdio.h>
// lecture d'un caractere saisi au clavier
main(){
int ch;
printf("tapez un caractere:\n");
ch = getc(stdin);
printf("caractere saisi est:%c\n", ch);
return 0;}



#############################################################################
#############################################################################



//05L06.c.....p67


#include <stdio.h>

main(){
/* afficher la longueur minimal de champ*/


	int num1, num2;
 	num1=12;
	num2=12345;
printf("affichage de la lonngueur minimal de champ\n");
printf("%d\n",num1);
printf("%5d\n",num2);
printf("%5d\n",num1);
printf("%05d\n",num1);
printf("%2d\n",num2);
return 0;
}

#############################################################################
#############################################################################

﻿//05L03.c.....p62// affichage d'un caractère a l'aide de putc




#include <stdio.h>

main(){

	int ch;

	ch=65; // valeur numerique de A

	printf(" caratere correspondant au code ASCII 65:\n");
	putc(ch, stdout   );
	printf("\n");
	return 0;}



#############################################################################
#############################################################################
//05L04.c.....p63 affichage de caractere à l'aide de putchar


#include <stdio.h>

main(){
	
	putchar(65);
	putchar(10);
	putchar(66);
	putchar(10);
	putchar(67);
	putchar(10);
	int ch;
	
	return 0;
	}
/*
./exe
A
B
C*/




#############################################################################
#############################################################################

﻿/* 05l05 page66

synthase: int printf(const char* format,...)
syntaxe de cours de cours: fonction printf()
#include <stdio.h>
int printf(constchar*format,.expression.);

*/
//

#include <stdio.h>
// lecture d'un caractere saisi au clavier
main(){


	printf("affichage des nombres en Hexadecimal majuscule, en hexadecimal miniscule puis en decimal\n");
	printf("Hex maj  Hex min decimal\n");
	printf("% X      % x     % d \n", 0,0,0);
	printf("% X      % x     % d \n", 1,1,1);
	printf("% X      % x     % d \n", 2,2,2);
	printf("% X      % x     % d \n", 3,3,3);
	printf("% X      % x     % d \n", 4,4,4);
	printf("% X      % x     % d \n", 5,5,5);
	printf("% X      % x     % d \n", 6,6,6);
	printf("% X      % x     % d \n", 7,7,7);
	printf("% X      % x     % d \n", 8,8,8);
	printf("% X      % x     % d \n", 9,9,9);
	printf("% X      % x     % d \n", 10,10,10);
	printf("% X      % x     % d \n", 11,11,11);
	printf("% X      % x     % d \n", 12,12,12);
	printf("% X      % x     % d \n", 13,13,13);
	printf("% X      % x     % d \n", 14,14,14);
	printf("% X      % x     % d \n", 15,15,15);

return 0;
}




#############################################################################
#############################################################################


//05L06.c.....p67


#include <stdio.h>

main(){
/* afficher la longueur minimal de champ*/


	int num1, num2;
 	num1=12;
	num2=12345;
printf("affichage de la lonngueur minimal de champ\n");
printf("%d\n",num1);
printf("%5d\n",num2);
printf("%5d\n",num1);
printf("%05d\n",num1);
printf("%2d\n",num2);
return 0;
}



#############################################################################
#############################################################################

﻿//05L07.c.....p66

#include <stdio.h>

main(){
// justification a droite et justification a gauche


	int num1, num2, num3,num4,num5;
	num1=1;
	num2=12;
	num3=123;
	num4=1234;
	num4=12345;
	num2=12345;
	printf("affiche les nombres: alignes a droites et a gauche\n");

	printf("%8d %-8d\n",num1,num1);
	printf("%8d %-8d\n",num2,num2);
	printf("%8d %-8d\n",num3,num3);
	printf("%8d %-8d\n",num4,num4);
	printf("%8d %-8d\n",num5,num5);
return 0;
}




#############################################################################
#############################################################################

﻿//05L08.c.....p70


#include <stdio.h>

main(){
/*  indicateur de precision */


	int int_num;
	double flt_num;
 	int num1=123;
	double flt_numn=123.456789;
printf("format entier par defaut:		%d\n", int_num);
printf(" avec indicateur de precision:	%2.8d\n",int_num);
printf("format decimal par defaut:		%f\n",flt_numn);
printf("avec indicateur de precision:	%-10.2f\n",flt_numn);
printf("%05d\n",num1);

return 0;
}




#############################################################################
#############################################################################


/*

*/



﻿/*6L01.c.....p66

syntaxe de cours:operande gauche= operande droit()
#include <stdio.h>
operande gauche= operande droit);
*/




#include <stdio.h>

main(){
// operateur arithmetique d'affection


	int x,y,z;
	x=1;
	y=3;
	z=10;

	printf("comme  x=%d, y=%d et z=%d,\n", x,y,z);

	x=x+y;
	printf("x=x+y affecte %d a x;\n", x);

	x=1;
	x+=y
    printf(" x += y affecte %d a x;\n", x);

	x=1;
	z=z*x+y;
	printf("z =z*x+y affecte %d a z;\n", z);

	z=10;
	z=z*(x+y);
	printf("z=z*(x+y) affecte %d a z;\n", z);

	z=10;
	z *= x+y;
	printf("z*=x+y affecte %d a z;\n", z);

return 0;
}



#############################################################################
#############################################################################


//06L02 p83


#include <stdio.h>

main(){
// longueur minimal de champ


int w,x,y,z, result;

	w=x=y=z=1;  // on a initialise a 1

	printf("comme  w=%d, x=%d, y=%d et z=%d,\n", w,x,y,z);

	result= ++w;

	printf("++w donne: %d,\n", result);

	result= x++;

	printf("x++ donne: %d,\n", result);
	
	result= --y;
	
	printf("--y donne: %d,\n", result);
	
	result= z--;
	
	printf(" z-- donne: %d,\n", result);
return 0;
}


#############################################################################
#############################################################################


﻿//06L03 p85


#include <stdio.h>

main(){
// resultat de produit par des expressions relationnelles


int x,y;
double z;
	x=7;                         // <  >
	y=25;
	z=24.46;



	printf("comme x= %d, y=%d et z=%d ,\n", x,y);

	printf("x >= y produit %d,\n", x >= y);

	printf("x==y   produit %d,\n",x==y);

	printf("x < z  produit %d,\n", x < z);

	printf("y > z  produit %d,\n", y > z);

	printf("x!=y-18 produit %d,\n", x!=y-18);

	printf("x+y !=z produit %d,\n", x+y !=z);
return 0;
}

/*
; ./exe
comme x= 7, y=25 et z=-1889785610 ,
x >= y produit 0,
x==y   produit 0,
x < z  produit 1,
y > z  produit 1,
x!=y-18 produit 0,
x+y !=z produit 1,
NB : s'agit de comparer les valeur
0 signifie que : c'est faux
1 signifie que : c'est vrai

*/


#############################################################################
#############################################################################


﻿//06L03 p87


#include <stdio.h>

main(){
// operateurs de conversions


int x,y;
	x=7;
	y=5;


	printf("comme x %d et y=%d,\n", x,y);


	printf("x/y donne %d \n", x/y);

	printf("(float) x/y donne %f \n", (float) x/y);
return 0;
}

#############################################################################
#############################################################################

/*07l01 page92

syntaxe de cour:  instruction for

#include <stdio.h>
for( expression1; expression2 ; expression3 ){
	instruction1; 
	instruction2;
    .
	.
	}
*/




#include <stdio.h>
// conversion en hexadecimal en utilisant la boucle for
main(){

    int i;

		printf("affichage des nombres en Hexadecimal majuscule, en hexadecimal miniscule puis en decimal\n");
		printf("Hex maj  Hex min decimal\n");

	for ( i = 0; i < 16; i++){
		printf("% X      % x     % d \n", i,i,i);
							}
	return 0;
		}




#############################################################################
#############################################################################
﻿//07l02 page95

#include <stdio.h>
// conversion en hexadecimal en utilisant la boucle for
main(){

    int i, j;


	for ( i = 0, j=8;  i < 8 ; j>16, i++, j-- ){

            

		printf("% d + % d=% d\n", i,j, i+j);
							}
	return 0;
		}





#############################################################################
#############################################################################

﻿/*07l03 page66

syntaxe de cour: creer une boucle infinie avec instruction for
#include <stdio.h>
for( ; ; ){
	// bloc d'instruction
}
*/




#include <stdio.h>
// instruction for avec expression multiples
main(){

    int i, j, result;


	for ( i = 0, j=1;  i < 8 ; i++, j++ ){
           
		printf("% d + % d=\n", i,j, j-i);
							}
	return 0;
		}





#############################################################################
#############################################################################


﻿/*07L04.....page 98

syntaxe de cours de cours: boucle for conditionnelle()

*/


#include <stdio.h>

main(){

	int c;
	printf(" entre un caractere\n(x pour quitter) \n" );
	for( c=' '; c != 'x';){
		
		c= getc ( stdin);
		putchar (c);
	}

	printf("\n vous venez de quitter la boucle for. A bientot!\n");
	
	return 0;
	}



#############################################################################
#############################################################################


﻿/*07L05.....page 100

#include <stdio.h>
syntaxe de cours de cours: boucle whilee()
for( expression ){
	instruction1;
	instruction2;
    .
	.
}
*/



#include <stdio.h>

main(){

	int c;
	c=' ';

	printf(" entrez un caractere\n(x pour quitter) \n" );
	while( c != 'x'){

		c= getc ( stdin);
		putchar (c);
	}
	
	
	
	

	printf("\n vous venez de quitter la boucle while. A bientot!\n");


	return 0;
	}



#############################################################################
#############################################################################

﻿/*07L04.....page 102

#include <stdio.h>
syntaxe de cours de cours: boucle whilee()
do( expression ){
	instruction1;
	instruction2;
    .
	.
	}while(expression );
*/



#include <stdio.h>

main(){

	int i;
	i=65;
	do{

	printf(" la valeur numerique de % c est % c \n", i,i);

	i++;
	}while( i<72);

	return 0;
	}



#############################################################################
#############################################################################

﻿/*07L07.....page 103

#include <stdio.h>
syntaxe de cours de cours: boucle imbiquées()
do( expression ){
	instruction1;
	instruction2;
    .
	.
	}while(expression );
*/



#include <stdio.h>

main(){

	int i, j;
	for (i=1; i<=3; i++){ // boucle externe
	printf(" iteration %d de la boucle externe.\n",i); 
	for (j=1; j<=4; j++			// boucle interne
	printf(" iteration %d de la boucle interne.\n",j);
	printf(" fin iteration %d de la boucle externe.\n",i);}

	return 0;
	}




#############################################################################
#############################################################################



/*08l01 page110

syntaxe de cour:  utilisation de l'operateur sizeof

#include <stdio.h>
sizeof (expression);
ex: var= sizeof (int);
	.
	}
*/


#include <stdio.h>

main(){

	char ch;
	int int_num;
	float flt_num;
	double dbl_num;

	printf(" taille de char: %d octets\n", sizeof (char));
	printf(" taille de ch: %d octets\n", sizeof ch);

	printf(" taille de int: %d octets\n", sizeof (int));
	printf(" taille de int_num: %d octets\n", sizeof int_num);

	printf(" taille de float: %d octets\n", sizeof (float));
	printf(" taille de flt_num: %d octets\n", sizeof flt_num);

	printf(" taille de double: %d octets\n", sizeof (double));
	printf(" taille de dbl_num: %d octets\n", sizeof dbl_num);


	return 0;
	}




#############################################################################
#############################################################################

/*08L02 page113

syntaxe de cour:  utilisation de l'operateur ET (&&)

#include <stdio.h>
exp1 && exp2;
exp1 et exp2 correspondant aux deux expresion evaluées;
	.
	}
*/


#include <stdio.h>

main(){

	
	int num;
	
	num=0;
	printf("valeur renvoyer par l'oprateur ET: %d\n",(num%2==0) && ( num%3==0));
	
	num=2;
	printf("valeur renvoyer par l'oprateur ET: %d\n",(num%2==0) && ( num%3==0));

	num=3;
	printf("valeur renvoyer par l'oprateur ET: %d\n",(num%2==0) && ( num%3==0));
	
	num=6;
	printf("valeur renvoyer par l'oprateur ET: %d\n",(num%2==0) && ( num%3==0));

	return 0;
	}




#############################################################################
#############################################################################


/*08L03 page115

syntaxe de cour:  utilisation de l'operateur ET (&&)

#include <stdio.h>
exp1¦¦ exp2;
exp1 et exp2 correspondent aux expresions evaluées;
	.
	}
*/


#include <stdio.h>

main(){

	
	int num;
	
	printf("entrez un chiffre divisible par 2 et 3\n");
	
	for (num=1; (num%2 !=0) || (num%3 !=0) ;)
	num= getchar () -48;
	printf("resultat: %d\n", num);

	return 0;
	}



#############################################################################
#############################################################################

/*08L04 page117

syntaxe de cour:  utilisation de l'operateur NON (!)

#include <stdio.h>
	!expresion;
expresion correspond a l'expresion traitée par l'operateur de negation;
	.
	}
*/


#include <stdio.h>
	
main(){

	
	int num;
	
	num=7;
	printf("num=7\n");
	
	printf("!(num<7) renvoie %d\n",!(num<7));

	printf("!(num>7) renvoie %d\n",!(num>7));
	
	printf("!(num==7) renvoie %d\n",!(num==7));

	return 0;
	}



#############################################################################
#############################################################################
/*08L05 page121

syntaxe de cour:  realisation d'un operateur bit a bit

#include <stdio.h>
	il se presente sur cette forme:
	X & Y
	X ¦ Y
	X ˆ Y
	  ~ X
	  X et Y sont des operandes.
	}
*/


#include <stdio.h>

main(){

	
	int x,y,z;
	x=4321;
	y=5678;
	
	printf("		x= %u ou 0X%04X\n", x, x);
	printf("		y= %u ou 0X%04X\n", y, y);
	
	z= x & y;
	printf("x & y renvoie		%6u ou 0X%04X\n", z, z);
	z= x|y;
	printf("x|y renvoie %6u ou 0X%04X\n",z,z);
	z= x^y;
	printf("x^y renvoie %6u ou 0X%04X\n", z,z);
	printf("~x renvoie %6u ou 0X%04X\n", ~ x, ~x);

	return 0;
	}


#############################################################################
#############################################################################

/*08L06 page123

syntaxe de cour:  operateur de decalage
	il se presente sur cette forme:
	X >> Y decales les bits d'un operande vers la droite 
	X <<Y
	
	}
*/


#include <stdio.h>

main(){


	int x,y,z;
	x=255;
	y=5;

	printf(" comme x= %4d ou 0X%04X\n", x, x);
	printf(" comme y= %4d ou 0X%04X\n", y, y);

	z= x >> y;
	printf("x>> y renvoie %6d ou 0X%04X\n", z, z);
	z = x << y;
	printf(" x << y renvoie %6u ou 0X%04X\n",z,z);            // pas d'espace dans  les format 0X%04X\n 

	return 0;
}




#############################################################################
#############################################################################


/*008L07 page124

syntaxe de cour:  operateur conditionnel
	il s'agit du seul operateur en C qui traite trois (03) operandes a la fois:
	X ? Y : z
		x contient la condition test y etz resentant la valeur finale de l'expression.
		selon que x renvoie une valeur differente de zero ou une valeur nulle. y ou z est
		choisie comme resultat.

*/


#include <stdio.h>

main(){


	int x;
	x = sizeof(int);

printf(" %s\n",
(x == 2) ? " taille de int est egal a 2 octets." :" taille de int differente de 2 octets");

	printf(" valeur maximal de int: %d\n",	(x != 2) ?  ~ (1<< x * 8-1) : ~( 1 << 15) );


	return 0;
}






/*

#include <stdio.h>

main(){


	int x;
	x = sizeof(int);

printf(" (x == 2) ?" );



printf(" taille de int est egal a %s\n octets." , x);




(x == 2) ? " taille de int est egal a 2 octets." :" taille de int differente de 2 octets");

	printf(" valeur maximal de int: %d\n",	(x != 2) ?  ~ (1<< x * 8-1) : ~( 1 << 15) );


	return 0;
}
*/






/*
exe:

taille de int differente de 2 octets
 valeur maximal de int: 2147483647      (suivant le processeur)



*/



#############################################################################
#############################################################################


/*009L01 page131

syntaxe de cour: modificateur signed et unsigned
	ipour les explicationsvoir page130 a 131
*/


#include <stdio.h>

main(){


	signed char ch ;
	int 		x ;
	unsigned int  y;

	ch = 0xFF;
	x=0xFFFF;
	y=0xFFFF;
	printf("valeur decimal de 0xFF signée : %d\n", ch);

	printf(" valeur decimal de 0xFFFF signée : %d\n", x );
	printf(" valeur decimal de 0xFFFF non signée : %u\n", y );
	printf(" valeur hexadecimal de 12345 : %X\n",12345);
	printf(" valeur hexadecimal de -12345 : %X\n", -12345 );


	return 0;
}



#############################################################################
#############################################################################


/*009L02 page134

syntaxe de cour: modification  de la taille de donnees
	modificateur de short et long
*/


#include <stdio.h>

main(){


	
	printf("taille de short int  : %d\n", sizeof (short int) );
	printf("taille de long  int: %d\n", sizeof (long int) );
	printf("taille de float : %d\n", sizeof (float) );
	printf("taille de double : %d\n", sizeof (double) );
	printf("taille de long double : %d\n", sizeof (long double) );

	return 0;
}



#############################################################################
#############################################################################

/*009L04 page137

syntaxe de cour: fonction mathematique
	calculer les valeurs trigonometrique: sin() cos() tangente()
*/


#include <stdio.h>
#include <math.h>
main(){

	double x;
	x = 45.0; 				// 45 degres
	x *= 3.141593 / 180.0 ; // convertir en radians 
	
	printf("sinus de 45 : %f\n", sin(x) );
	printf("cosinus de 45 : %f\n", cos(x) );
	printf("tangente de 45 : %f\n", tan(x) );
	
	return 0;
}




#############################################################################
#############################################################################

/*009L05 page139

syntaxe de cour: fonction mathematique
	fonction pow() et sqrt ()
	#include <math.h>
	double pow ( double x, double y)
	.
	.
	sqrt ( double x)
*/


#include <stdio.h>
#include <math.h>
main(){

	double x, y, z;
	x = 64.0; 	
	y=3.0; 
	z=0.5;
	
	printf(" pow ( 64.0 , 3.0 ) donne %7.0f\n", pow( x, y) );
	printf(" sqrt ( 64.0 ) donne %2.0f\n", sqrt( x) );
	printf(" pow ( 64.0 , 0.5 ) donne %2.0f\n", pow( x, z) );
	
	return 0;
}

#############################################################################
#############################################################################

/*10L01 page145

syntaxe de cour: instructions conditionnelles 
	instructions conditionnelles if
	if(expression)
	instruction1;
	instruction2;
	.
	.
	
*/


#include <stdio.h>
main(){

	int i;
	
	printf(" entier de 0 a 100 divisible\n");
	printf(" par 2 et 3:\n");
	
	for(i=0; i <= 100; i++ )
	if ((i % 2 == 0)&& (i % 3 == 0))
	
	printf ("	% d \n", i);	
	return 0;
}




#############################################################################
#############################################################################



/*10L02 page146

syntaxe de cour: instructions conditionnelles
	instructions conditionnelles if-else
	if(expression){
	instruction1;
	instruction2;
	.
	.
	} else {
	instruction_A;
	instruction1_B;
			}
*/


#include <stdio.h>
main(){

	int i;

	printf(" nombre pair 	nombre impair\n");

	for(i=0; i <= 10; i++ )
	if (i%2 == 0)
	printf ("%d\n", i);
    else
	printf ("%14d\n", i);
	return 0;
}


#############################################################################
#############################################################################

/*10L02 page146

syntaxe de cour: instructions conditionnelles
	instructions conditionnelles if-else
	if(expression){
	instruction1;
	instruction2;
	.
	.
	} else {
	instruction_A;
	instruction1_B;
			}
*/


#include <stdio.h>
main(){

	int i;

	printf(" nombre pair 	nombre impair\n");

	for(i=0; i <= 10; i++ )
	if (i%2 == 0)
	printf ("%d\n", i);
    else
	printf ("%14d\n", i);
	return 0;
}







#############################################################################
#############################################################################

/*10L03 page148

syntaxe de cour: instructions conditionnelles
	instructions conditionnelles if imbriquées
*/


#include <stdio.h>
main(){

	int i;
	for(i= -5; i <= 5; i++ )
	if (i > 0)
    if (i%2 == 0)


	printf(" %d est un  nombre pair\n", i);
		else
		printf(" %d est un  nombre impair\n", i);

		else if (i == 0 )
		printf(" valeur nul\n", i);
	else
	printf ("nombre negatif:	% d \n", i);
	return 0;
}






#############################################################################
#############################################################################

/*10L04 page150

syntaxe de cour: instructions conditionnelles 
	instructions conditionnelles switch
	switch expression{
	case expression1:
		instructions1;
		case expression2:
		instructions2;
		.
		.
		default:
		instruction-par-defaut;
	}
*/


#include <stdio.h>
main(){

	int day;
	
	printf("tapez un numero de jour \n");
	printf("compris entre 1 et 3\n");
	
	day = getchar();
	switch (day){
	case'1':
	printf(" jour 1\n ");
		case'2':
	printf(" jour 2\n ");
			case'3':
	printf(" jour 3\n");

	default:
		;
		}
	return 0;
}






#############################################################################
#############################################################################
/*10L05 page150

syntaxe de cour: instructions conditionnelles
	instructions conditionnelles break




*/


#include <stdio.h>
main(){

	int day;

	printf("tapez un numero de jour \n");
	printf("compris entre 1 et 7\n");

	day = getchar();
	switch (day){
	case'1':
	printf(" jour: dimanche  \n ");
	break;

	case'2':
	printf(" jour: lundi \n ");
	break;

	case'3':
	printf(" jour: mardi  \n ");
	break;

	case'4':
	printf(" jour: mercredi \n ");
	break;

	case'5':
	printf(" jour: jeudi  \n ");
	break;

	case'6':
	printf(" jour: vendredi \n ");
	break;

	case'7':
	printf(" jour: samedi  \n ");
	break;

	default:

	printf(" erreur: le nombre se situe hors intervalle\n ");
	break;
	}
	return 0;
}


#############################################################################
#############################################################################


/*10L06 page153

syntaxe de cour: instructions conditionnelles 
	sortie d'une boucle infinie 
	for (; ;){
		instructions1;
		instructions2;
		.
		.
		}
		while (1){
		instructions1;
		instructions2;		
	}
*/


#include <stdio.h>
main(){

	int c;
	printf("tapez un caractere \n ( x pour quiter )\n");
	while (1){
	c = getc (stdin);
	if ( c == 'x')
	break;
	}
	printf("vous venez de quitter une boucle while infinie. a bientot! \n");
	
	return 0;
}



#############################################################################
#############################################################################


/*10L07 page155

syntaxe de cour: instructions conditionnelles
	instruction continue

*/


#include <stdio.h>
main(){

	int i, sum;
	
	sum=0;
	
	for (i=1; i<8; i++){
	if (( i == 3)|| (i == 5))
	continue;
	
	sum += i;
	}
	printf("somme de 1,2,3,4,5,6 et 7: %d\n",sum);

	return 0;
}



#############################################################################
#############################################################################
/* 11L01.c.....p163

*/ 


#include <stdio.h>

main(){
	 char   c;
	 int    x;
	 float  y;
 	 printf("c:adresse=0x%p; contenu=%c \n",&c, c);
 	 printf("x:adresse=0x%p; contenu=%d \n",&x, x);
 	 printf("y:adresse=0x%p; contenu=%5.2\n",&y, y);

	 c='A';
	 x=7;
	 y=123.45;
	 printf("c: adresse=0x%p; contenu=%c\n",&c, c);
	 printf("x: adresse=0x%p; contenu=%d\n",&x, x);
	 printf("y: adresse=0x%p; contenu=%5.2f\n",&y, y);

	return 0;
}





/*

c:adresse=0x0x7fff4a865e4f; contenu= 
x:adresse=0x0x7fff4a865e48; contenu=0 
y:adresse=0x0x7fff4a865e44; contenu=%5.2
c: adresse=0x0x7fff4a865e4f; contenu=A
x: adresse=0x0x7fff4a865e48; contenu=7
y: adresse=0x0x7fff4a865e44; contenu=123.45

*/


#############################################################################
#############################################################################

/* 11L02.c.....p165
char  *ptr_c; 	// pointeur vers un caractere 
int   *ptr_int; // pointeur vers un entier 
float *ptr_flt; // pointeur vers un nombre decimal

*/ 


#include <stdio.h>

main(){
	char   c, *ptr_c;
	int    x, *ptr_x; 
	 float  y, *ptr_y;
	 
	 c='A';
	 x=7;
	 y= 123.45;
	 
 	 printf("c: adresse=0x%p; contenu=%c \n",&c, c);
 	 printf("x: adresse=0x%p; contenu=%d \n",&x, x);
 	 printf("y: adresse=0x%p; contenu=%5.2f\n",&y, y);
	 
	 ptr_c=&c;
	 printf("ptr_c: adresse=0x%p, contenu=0x%p\n", &ptr_c, ptr_c);
	 printf("*ptr_c => %c\n", *ptr_c);
	 
	 ptr_x = &x;
	 printf("ptr_x: adresse= 0x%p, contenu= 0x%p\n", &ptr_x, ptr_x);
	 printf("*ptr_x => %d\n", *ptr_x);
	 
	 ptr_y=&y;
	 printf("ptr_y: adresse= 0x%p, contenu= 0x%p\n", &ptr_y, ptr_y);
	 printf("*ptr_y => %5.2f, %p\n", *ptr_y);


	 
	return 0;
}


/*
gcc -o  exe   11L02.c ; ./exe


*/



/*co



c: adresse=0x0x7ffd47faebaf; contenu=A 
x: adresse=0x0x7ffd47faeb9c; contenu=7 
y: adresse=0x0x7ffd47faeb8c; contenu=123.45
ptr_c: adresse=0x0x7ffd47faeba0, contenu=0x0x7ffd47faebaf
*ptr_c => A
ptr_x: adresse= 0x0x7ffd47faeb90, contenu= 0x0x7ffd47faeb9c
*ptr_x => 7
ptr_y: adresse= 0x0x7ffd47faeb80, contenu= 0x0x7ffd47faeb8c
*ptr_y => 123.45, 0x7fffffc4


*/










#############################################################################
#############################################################################

/* 11L03.c.....p168
syntaxe de cours: mises a jour de variable par pointeur
modification de valeurs par pointeurs 

*/ 


#include <stdio.h>

main(){
	 char   c, *ptr_c;
	 
	 
	 c='A';
	 
	 
 	 printf("c: adresse=0x%p; contenu=%c \n",&c, c);
	 
	 ptr_c = &c ;
	 printf("ptr_c: adresse=0x%p, contenu=0x%p\n", &ptr_c, ptr_c);
	 printf("*ptr_c => %c\n", *ptr_c);
	 
	 *ptr_c = 'B' ;
	 printf("ptr_c: adresse=0x%p, contenu=0x%p\n", &ptr_c, ptr_c);
	 printf("*ptr_c => %c\n", *ptr_c);
	 printf("c: adresse=0x%p, contenu=0x%p\n", &c, c);

	return 0;
}

/*

NB: >  fait référence

--------------------------------
L'opérateur d'indirection *

l'asterixque    *  porte le nom d'operateurd'indirection( ou d'adressage indirecte) lorsqu'il est utilisé comme comme operateur unaire.

	Il est possible de référencer la valeur d'une variable  en combinant l'operateur * et 'l'operande ( l'élément) contenant  l'adresse de la variable  

Ainsi: 

- avec :  char   x, *ptr_x;
 
l' expresion *ptr_x   >  la valeur de la variable x affectée à   au pointeur ptr_x



Ainsi, 

si x  = 1234, 

on peut  

	- affecter la valeur gauche (adresse)  de x à ptr_x
sous forme : ptr_x = &x


	Donc  l'expression *ptr_x   >   1234  correspondant à la valeur droite (contenu)  de x 
	
	

RESUME:

					x=1234
valeur gauche (adresse de x)				valeur droite (contenu)

	ptr_x = &x			|		*ptr_x   >   1234
	
	

NB:  Pour clarifier la terminologie


   *ptr_x  est une  expresion d'indirection de pointeur : 

        et ptr_x est le pointeur  ( la variable pointeur). 


*/




#############################################################################
#############################################################################

/* 11L03.c.....p168
syntaxe de cours: mises a jour de variable par pointeur
modification de valeurs par pointeurs 

*/ 


#include <stdio.h>

main(){
	 char   c, *ptr_c;
	 
	 
	 c='A';
	 
	 
 	 printf("c: adresse=0x%p; contenu=%c \n",&c, c);
	 
	 ptr_c = &c ;
	 printf("ptr_c: adresse=0x%p, contenu=0x%p\n", &ptr_c, ptr_c);
	 printf("*ptr_c => %c\n", *ptr_c);
	 
	 *ptr_c = 'B' ;
	 printf("ptr_c: adresse=0x%p, contenu=0x%p\n", &ptr_c, ptr_c);
	 printf("*ptr_c => %c\n", *ptr_c);
	 printf("c: adresse=0x%p, contenu=0x%p\n", &c, c);
	 
	 
	return 0;
}




/*co



$ gcc -o  exe   11L03.c ; ./exe
c: adresse=0x0x7ffda2d4c6cf; contenu=A 
ptr_c: adresse=0x0x7ffda2d4c6c0, contenu=0x0x7ffda2d4c6cf
*ptr_c => A
ptr_c: adresse=0x0x7ffda2d4c6c0, contenu=0x0x7ffda2d4c6cf
*ptr_c => B
c: adresse=0x0x7ffda2d4c6cf, contenu=0x0x42

*/
#############################################################################
#############################################################################

/* 11L04.c.....p170
syntaxe de cours: pointeurs multiples
pointeurs multiples
*/


#include <stdio.h>

main(){

	 int    x;
	 int   *ptr_1,*ptr_2,*ptr_3;


	 x=1234;


 	 printf("x: adresse = 0x%p; contenu = %d\n", &x, x);

	 ptr_1 = &x ;
	 printf("ptr_1: adresse = 0x%p, contenu = 0x%p\n", &ptr_1, ptr_1);
	 printf("*ptr_1 => %d\n", *ptr_1);

	 ptr_2 = &x ;
	 printf("ptr_2: adresse=0x%p, contenu=0x%p\n", &ptr_2, ptr_2);
	 printf("*ptr_2 => %d\n", *ptr_2);

	 ptr_3 = ptr_1 ;
	 printf("ptr_3: adresse = 0x%p, contenu = 0x%p\n", &ptr_3, ptr_3);
	 printf("*ptr_3 => %d\n", *ptr_3);

	return 0;
}

/*c

$ gcc -o  exe   11L04.c ; ./exe
x: adresse = 0x0x7ffec4a0bb9c; contenu = 1234
ptr_1: adresse = 0x0x7ffec4a0bb90, contenu = 0x0x7ffec4a0bb9c
*ptr_1 => 1234
ptr_2: adresse=0x0x7ffec4a0bb88, contenu=0x0x7ffec4a0bb9c
*ptr_2 => 1234
ptr_3: adresse = 0x0x7ffec4a0bb80, contenu = 0x0x7ffec4a0bb9c
*ptr_3 => 1234


*/




#############################################################################
#############################################################################

/* 12L01.c.....p177
syntaxe de cours: declarer un tableau
initialisation d'un tableau
type nom[taille] ;
exp: int tableau_entier [8];

*/


#include <stdio.h>

main(){

	 int i;
	 int    list_int [10];
	 for ( i=0; i < 10; i++){
	 list_int  [i] = i + 1;

 	 printf("list_int  [%d] contient %d\n", i, list_int [i] );
}

	return 0;
}




#############################################################################
#############################################################################


/* 12L02.c.....p178
syntaxe de cours: declarer un tableau
calculer la taille d'un tableau
type nom[taille] ;
avec l'expression suivante calculons la la taille d'un talbleau
sizeof (type) *taille

*/


#include <stdio.h>

main(){

	 int total_byte ;
	 int    list_int [10];
	 total_byte= sizeof (int) *10;
	  	 printf("taille de int:   %d octets\n",sizeof (int ) );
	  	 printf("taille total du tableau:    %d octets\n",total_byte );
	  	 printf("adresse du premier element:    0x%x\n", &list_int [0] );
	  	 printf("adresse du dermier element:    0x%x\n", &list_int [9] );

	return 0;
}










#############################################################################
#############################################################################


/* 12L03.c.....p181
syntaxe de cours: tableau et pinteur 
referencer un tableau a l'aide d'un  pointeur
char *ptr_c;
char list_c [10];
ptr_c = list_c;
la varaible ptr_c fait reference au tableau a l'aide de l'adresse
de debut,correspondant au premier element du tableau list_c

*/


#include <stdio.h>

main(){
	int *ptr_int;
	int list_int [10];
	int i;
	
	for (i=0; i<10; i++)
	list_int [i] = i + 1;
	printf("adresse de debut du tableau:   0x%p octets\n", ptr_int);
	printf("valeur du premier element:   %d octets\n",*ptr_int );
	
	ptr_int = &list_int  [0];
	printf("adresse du premier element:    0x%x\n", ptr_int );
	printf("valeur du premier element:   %d octets\n",*ptr_int );

	return 0;
}









#############################################################################
#############################################################################
/* 12L04.c.....p182
syntaxe de cours: affficher un tableau de caratere  
afffichage d'un tableau de caratere  char *ptr_c;

*/


#include <stdio.h>

main(){
	char array_ch [8] = {'B','O','N','J','O','U','R', '\0'};
	int i;
	
	for (i=0; i<8; i++)
	
	printf("array_ch [%d] contient:   %c\n", i, array_ch [i]);
							
							/* METHODE I */
							
	printf("regroupement des elements (methode I):\n");
	
	for (i=0; i<8; i++)
	
	printf(" %c", array_ch [i]);
							
							/* METHODE II */
							
	printf("\nregroupement des elements (METHODE II): \n");
	
	printf("%s\n", array_ch);
	return 0;
}






############################################################################
############################################################################


/* 12L05.c.....p184
syntaxe de cours:Le caratere nul (\0)  
arret du traitement par le caratere nul

*/


#include <stdio.h>

main(){
	char array_ch [15] = {'c','i','s','p','O','w','e','r','f','u','l','!','\0'};
	int i;
	
	for (i=0; array_ch [i]; i++)
	
	printf(" %c", array_ch [i]);
	return 0;
	}
	
	
	
	/*
	$ . with     printf(" %c  \n", array_ch [i]);
 c  
 i  
 s  
 p  
 O  
 w  
 e  
 r  
 f  
 u  
 l  
 !  


*************

$ ./a   with   printf(" %c", array_ch [i]);
 c i s p O w e r f u l 
 
 
 
 
	
	
	*/


#############################################################################
#############################################################################
j/* 12L06.c.....p186
syntaxe de cours:Tableau de plusieurs dimensions 
affichage du contenu, du tableau a deux dimensions
type  Nom {taille 1] {taille 2........{taille Nom]
]

*/


#include <stdio.h>

main(){
	int two_dim [3] [5] = { 1,2,3,4,5,10,20,30,40,50,100,200,300,400,500 };
	int i,j;
	
	for (i=0; i<3; i++){
	printf(" \n");
	for (j=0; j<5; j++)
	printf(" %6d",two_dim [i][j]);
	}
	return 0;
	}





#############################################################################
#############################################################################

/* 12L07.c.....p187
syntaxe de cours: Tableau non dimensionné
initialisation de deux tableaux non dimensionné

*/


#include <stdio.h>

main(){
	char array_ch [15] = {'c','i','s','p','O','w','e','r','f','u','l','!','\0'};
	int list_int  []  [3] ={ 1,1,1, 2,2,8, 3,9,27, 4,16,64, 5,25,125, 6,36,216, 7,49,343};
	printf("Taille de array_ch [] : %d octets\n", sizeof (array_ch));
	printf("Taille de list_int [][3] : %d octets\n", sizeof (list_int));

	return 0;
	}
// okk




#############################################################################
#############################################################################

/*
. 13. gestion de chaînes
*/


/* 13L01.c ............... page 196
chaine constante et constante caractere
initialisation des chaines
char ch= 'x';
char str []= 'x';


*/


#include <stdio.h>


int main(){
	char str1[] ={'u','n','e',' ','c','h','a','i','n','e',' ','c','o','n','s','t','a','n','t','e','\0'};
        char str2[] = "autre constante caractere";
		char *ptr_str;
		int i;
        /* affichage de str2 */
        for (i=0; str1[i]; i++)
		printf (" %c ", str1[i] );
				printf ("\n");
        /* affichage de str2 */
        for (i=0; str2[i]; i++)
		printf (" %c ", str2[i] );
				printf ("\n");
        /* affectation d'une chaine a un pointeur */
        ptr_str= "affectation d'une chaine a un pointeur." ;
        for (i=0; *ptr_str; i++)
		printf (" %c ", *ptr_str++ );

        return 0;
}
// okk




#############################################################################
#############################################################################

/* 13L02.c ............... page 198
longueur d'une chaine
fonction strlen ()
mesurer la taille d'une chaine
#include <string.h>

size_t strlen ( const char *s);
*/


#include <stdio.h>
#include <string.h>

int main(){
	char str1[] ={'u','n','e',' ','c','h','a','i','n','e',' ','c','o','n','s','t','a','n','t','e','\0'};
        char str2[] = "autre chaine constante caractere";
		char *ptr_str = "affecter une chaine a un pointeur.";



		printf ("longueur de str1: %d octets \n ", strlen (str1) );


		printf ("longueur de str2: %d octets \n ", strlen (str2) );


		printf ("longueur de la chaine affectee ptr_str : %d octets \n ", strlen (ptr_str) );

        return 0;
} // okk




#############################################################################
#############################################################################

/* 13L03.c...copie de chaine....page 200
*/

#include <stdio.h>
#include <string.h>

int main(){
char str1[] = ".copie d'une chaine.";
char str2[19];
char str3[19];
int i;
/* avec strcpy()
*/
strcpy(str2,str1);
/* sans strcpy()
*/
for (i=0; str1[i]; i++)
str3[i]=str1[i];
str3[i]='\o';
/* affichage de str2 etstr3
*/
printf ("contenu de str2 %s:\n",str2 );
printf ("contenu de str3 %s:\n",str3 );
 return 0 ;
 }






#############################################################################
#############################################################################
/* 13_L04 fonction  gets() et puts()....page 202
*/
#include <stdio.h>

main(){

char str[80];
int i;

printf ("taper une chaine de 79 caracteres maximum :\n");
getc(str) ;
i=0;
while (str[i]){
if ((str[i]>=97) && (str[i]<=122))
str[i]-= 32 ;/* conversion en majuscules*/
 ++i;}
printf ("la chaine est ( en majuscule) :\n");
 puts(str) ;
 return 0 ;
 }


#############################################################################
#############################################################################
/* 13L05.c......................page 204
indicateur %s
fonction scanf ()

*/


#include <stdio.h>
#include <math.h>

int main() {
        char  str[80];
        int x, y;
		float z;



		printf (" entrez deux entiers :\n " );
		scanf ("%d   %d",&x, &y);
		printf (" entrez un nombre a virgule  flottante:  \n " );

		scanf ("%f", &z);
		printf (" entrez dune chaine :\n " );
		scanf ("%s", str);

		printf (" voici ce que vous avez saisi :\n " );
		printf ("%d   %d\n %f\n %s\n",x, y, z, str);



        return 0 ;
} //okk


#############################################################################
#############################################################################
/*

14 portée et classe d’enregistrement
*/


/* 14L01.c......................page 211
affichage de variable de portée differente
portée locale
int main () {
int i;          // portée locle
.
.
.
return 0;
}

*/


#include <stdio.h>
#include <math.h>

int main() {

        int i=32; // portée du block 1
		printf ("bloc externe: i=%d \n", i );
		{
				/*debut du bloc interne */

		int i, j;       // portée du bloc 2
					/* fin de bloc interne */
		printf ("bloc interne: \n");

		for (i=0, j=10;i<=10; i++, j--)

            printf (" i=%2d , j=%2d\n ", i, j);




		}

			/* fin de bloc interne */
		printf ("bloc externe: i=%d \n ", i );
        return 0 ;
}// okk





#############################################################################
#############################################################################
/* 14L02.c......................page 213
relation entre variable globale et variable locales. portée globale et portée locale


int x=0;           // portée globale
float y=0.0;       // portée globale

int main () {
int i;          // portée locle
.
.
.
return 0;
}

*/


#include <stdio.h>




        int x=1234;              // portée locale
		double y=1.234567;

		void function_1()
		{
		printf("function_1: \n x=%d, y=%f\n", x,y);
		}
		
        main() {
		int x=4321;           // portée locale
		function_1();
		printf ("bloc principal: \n x=%d, y=%f\n", x,y);

		/* bloc imbriquée */
		{

 		double y=7.654321;

        function_1();
		printf ("bloc imbrique: \n x=%d y=%f ",x,y );

		}

printf ("\n \n");

        return 0 ;
}
// OKK





#############################################################################
#############################################################################
/* 14L03.c......................page 216
indicateur de classe de stockage
indicateur auto
indicateur static
int main () {
int i;           // portée locale et durée limitée
static int j;      // portée locale et durée permanente
.
.
return 0;
}

*/

#include <stdio.h>
        int add_two (int x, int y)
		{
		static int counter = 1;
		printf("appel de fonction : %d,\n", counter++);
		return (x + y);
}
	 main() {
		int i,j;
		for (i=0, j=5; i<5; i++, j--)
	printf ("%d + %d donne %d.\n", i,j, add_two(i,j));
        return 0 ;
}
// okk





#############################################################################
#############################################################################

/* 14L03.c......................page 216
indicateur de classe de stockage
indicateur auto
indicateur static
int main () {
int i;           // portée locale et durée limitée
static int j;      // portée locale et durée permanente
.
.
return 0;
}

*/


#include <stdio.h>




        int add_two (int x, int y)
		{
		static int counter = 1;


		printf("appel de fonction : %d,\n", counter++);
		return (x + y);
}

	 main() {
		int i,j;
		for (i=0, j=5; i<5; i++, j--)


		printf ("%d + %d donne %d.\n", i,j, add_two(i,j));



        return 0 ;
}
// okk




#############################################################################
#############################################################################
 /*15. fonction c…*/

/* 15L01.c......................page 229
appeler une fontion
prototype:
type  nom_de_fonction (
type argument1,
type argument2,
type argument3,
.
.
type argumentN,
);
*/


#include <stdio.h>

        int fonction_1 (int x, int y);
		double fonction_2(double x, double y)
		{
		printf(" fonction_2 en cours d'execution.\n");


		return (x - y);
}

	 main() {
		int x1 =80;
		int y1 =10;
		double x2 = 100.123456;
		double y2 = 10.123456;

		printf(" argument de fonction_1: %d et %d .\n",x1, y1);

		printf("valeur de renvoi: %d.\n",fonction_1(x1, y1) );

		printf(" argument de fonction_2: %f et %f .\n", x2, y2);

        printf(" valeur de renvoi : %f.\n", fonction_2(x2, y2));

        return 0 ;
	}
		int fonction_1 (int x, int y)
		{
		printf("fonction_1 en cours d'execution.\n");

				return (x + y);


		}

// okk



#############################################################################
#############################################################################

/* 15L02.c......................page 232
 fontion sans argument
int c;
c = getchar();
// la fontion getchar peut etre declare ainsi
int getchar(void);
*/


#include <stdio.h>
#include <time.h>

		void GetDateTime (void);


	 main() {

		printf(" Intruction avant l'appel de GetDateTime().\n");
		GetDateTime();

		printf(" Intruction apres l'appel de GetDateTime().\n");
  return 0 ;
}

		void GetDateTime (void)
		{
		time_t now;

		printf("  GetDateTime() en cours d'execution.\n");

			time(&now);



		printf(" date et heure actuelles: %s\n",asctime (localtime(&now)));

		} // okk








#############################################################################
#############################################################################

/* 15L03.c......................page 237
traiter des  arguments en nombre variables.
syntaxe:
#include <stdarg.h>
void va_start ( va_list ap, fix);
syntaxe:
#include <stdarg.h>
type va_arg (va_list ap, type-donnees
*/


#include <stdio.h>
#include <stdarg.h>

		double AddDouble (int x, ...);


 main() {

		double d1 =  1.5;
		double d2 =  2.5;
		double d3 =  3.5;
		double d4 =  4.5;

		printf(" argument: %2.1f\n", d1);
		printf(" resultat de l' addition: %2.1f\n\n", AddDouble (1,d1) );

		printf(" argument: %2.1f et %2.1f \n " , d1, d2);
		printf(" resultat de l' addition: %2.1f\n\n" , AddDouble ( 2, d1, d2) );

		printf(" argument: %2.1f, %2.1f et %2.1f \n", d1, d2, d3);
		printf(" resultat de l' addition: %2.1f \n \n " , AddDouble ( 3, d1, d2, d3) );

		printf(" argument: %2.1f, %2.1f, %2.1f et %2.1f \n ", d1 , d2, d3, d4);
		printf(" resultat de l' addition: %2.1f\n\n", AddDouble (4,d1, d2, d3, d4) );

return 0 ;
}
		double AddDouble (int x, ...)
		{
		va_list arglist;
		int i;
		double result = 0.0;

		printf(" nombre d'argumens: %d \n ", x);

		va_start (arglist, x) ;
		for ( i=0; i<x; i++)
		result += va_arg (arglist, double);
		va_end (arglist);

return result;
		}
		// ok







#############################################################################
#############################################################################

 /*
 
 …*/
 

/*16L01.c......................page 244

arithmetique des pointeurs
taille scalaire des pointeurs

*/


#include <stdio.h>

 main() {

		char *ptr_ch;
		int *ptr_int;
		double *ptr_db;

					/* pointeur char ptr_ch;*/
		printf(" position actuelle de ptr_ch: 0X%p \n",ptr_ch);

		printf(" position apres ptr_ch + 1: 0X%p \n", ptr_ch + 1 );
		printf(" position apres ptr_ch + 2: 0X%p \n " , ptr_ch + 2);

		printf(" position apres ptr_ch - 1: 0X%p \n " , ptr_ch - 1);
		printf(" position apres ptr_ch - 2: 0X%p \n " , ptr_ch - 2);

					/* pointeur int ptr_int */
		printf(" position actuelle de ptr_int: 0X%p \n",ptr_int);

		printf(" position apres ptr_int + 1: 0X%p \n", ptr_int + 1 );
		printf(" position apres ptr_int + 2: 0X%p \n " , ptr_int + 2);

		printf(" position apres ptr_int - 1: 0X%p \n " , ptr_int - 1);
		printf(" position apres ptr_int - 2: 0X%p \n " , ptr_int - 2);

							/* pointeur double ptr_ch */
		printf(" position actuelle de ptr_db: 0X%p \n", ptr_db);

		printf(" position apres ptr_db + 1: 0X%p \n", ptr_db + 1 );
		printf(" position apres ptr_db + 2: 0X%p \n " , ptr_db + 2);

		printf(" position apres ptr_db - 1: 0X%p \n " , ptr_db - 1);
		printf(" position apres ptr_db - 2: 0X%p \n " , ptr_db - 2);



return 0;
		}
		// ok







#############################################################################
#############################################################################
/* 16L02.c......................page 247
soustraction de pointeurs

*/


#include <stdio.h>

 main() {

		int *ptr_int1, *ptr_int2;

					/* pointeur char ptr_ch;*/
		printf(" position de ptr_int1: 0X%p \n",ptr_int1);

		ptr_int2 = ptr_int1 + 5;

		printf(" position de  ptr_int2 = ptr_int1 + 5: 0X%p \n", ptr_int2  );

		printf(" soustraction de ptr_int2 - ptr_int1: %d \n " , ptr_int2 - ptr_int1);

		ptr_int2 = ptr_int1 -5;

		printf(" position de ptr_int2 = ptr_int1 -5: 0X%p \n " , ptr_int2);

		printf(" soustraction de ptr_int2 - ptr_int1: %d \n " , ptr_int2 - ptr_int1);

return 0;
		}
		// ok








#############################################################################
#############################################################################

/* 16L03.c......................page 249
arithmetique des pointeurs
lecture de tableau à l'aide de pointeurs

*/


#include <stdio.h>

 main() {

		char str[] = "It's a string!";
		char *ptr_str;
		int List[] = {1, 2,3, 4, 5};
		int *ptr_int;

					/* lecture du tableau char */
		ptr_str = str;
		printf(" avant  mise a jour, str contient: %s\n",str);

		printf(" avant  mise a jour, str[5] contient : %c\n",str[5]);
		*(ptr_str+5) = ' A';
		printf(" apres  mise a jour, str[5] contient : %c\n", str[5]);
		printf(" apres  mise a jour, str contien: %s\n", str);

					/* lecture du tableau int */
		ptr_int = List;
		printf(" avant  mise a jour, List[2] contient : %d\n",List[2]);

					*(ptr_int + 2 )= - 3;
		printf(" apres la mise a jour, List[2] contient : %d\n", List[2]);

return 0;
		}
		// OK







#############################################################################
#############################################################################
/* 16L04.c......................page 251
 pointeur et fontion
transmission des tableaux en arguments
*/


#include <stdio.h>


		int AddThree(int list[]);


	 main() {
		int sum, list[3];
		printf(" Entrez trois entiers :\n ");
		scanf("%d%d%d", &list[0],&list[1],&list[2]);
		sum=AddThree (list);
		printf(" l'addition donne : %d\n", sum);
        return 0 ;
		}

		int AddThree(int list[])
		{
		int i;
		int result =0;
		for ( i=0; i<3; i++)
		result += list[i];
		return result;
		}
		//ok

#############################################################################
#############################################################################


/* 16L05.c......................page 252
passer des pointeurs vers des fonctions
transmission des tableaux et arguments
*/


#include <stdio.h>


void ChPrint (char *ch);
int DataAdd(int *List, int max);

	 main() {
		char str[] = "It's a string!";
		char *ptr_str;
		int List[5] = {1, 2,3, 4, 5};
		int *ptr_int;

		/* affecter l'adresse  au pointeurs */
		ptr_str = str;
		ChPrint(ptr_str);
		ChPrint(str);


		/* affecter l'adresse  au pointeurs */
		ptr_int = List;
		printf(" somme renvoyee par DataAdd() : %d\n",DataAdd(ptr_int,5));
		printf(" somme renvoyee par DataAdd() : %d\n",DataAdd(List,5));

	return 0;
			 }

		void ChPrint(char *ch)
		{
		printf(" %s\n", ch);
		}
		int DataAdd(int *List, int max)
		  {

			int i;
			int sum = 0;
			for (i = 0; i<max; i++)
            sum += List[i];
return sum	;
		  }
		// OK







#############################################################################
#############################################################################

/* 16L06.c......................page 254
passer des tableaux a plusieurs dimensions
*/


#include <stdio.h>

     		/* declarartion */

int DataAdd1(int list[][5], int max1, int max2);
int DataAdd2(int *list, int max1, int max2);

		/* main() fonction */
	 main() {
		int list[2][5] = {1, 2,3, 4, 5,5,4, 3, 2, 1};

		int *ptr_int;


		printf(" total renvoye par DataAdd1(): %d\n",DataAdd1(list,2,5));
		ptr_int=&list[0][0];
		printf(" total renvoye par DataAdd2(): %d\n",DataAdd2(ptr_int, 2, 5));

return 0;
			 }

					/* definitions */

		int DataAdd1(int list[][5], int max1, int max2)
		{
int i, j;
		int sum = 0;
		for (i = 0; i<max1; i++)
		for (j = 0; j<max2; j++)
        sum += list[i][j];
return sum	;
		}
					/* definitions */

		int DataAdd2(int *list, int max1, int max2)
		  {
int i, j;
		int sum = 0;
		for (i = 0; i<max1; i++)
		for (j = 0; j<max2; j++)
        sum +=  *(list +i *max2 +j);
return sum	;

		  }
		// ok

#############################################################################
#############################################################################

/* 16L07.c......................page 257
tableaux de pointeurs
*/

#include <stdio.h>

     		/* declarartion */

void StrPrint1( char **str1, int size);
void StrPrint2( char *str2);

		/* main() fonction */
	 main() {
		char *str[4] = {"There's music in the sighing of et reed;",
						"There's music in the gushing of a rill;",
						"There's music in all thigns if men had ears;",
						"there earth is but an echo of the spheres.\n"
						};

		int i, size;
		size = 4;
		StrPrint1( str, size);
		for (i=0; i<4; i++)
		StrPrint2( str[i]);

return 0;
		    }

					/* definitions */

void StrPrint1( char **str1, int size)
		  {
		int i;
		for (i = 0; i<size; i++)
		printf ("%s\n", str1[i]);
		  }
					/* definitions */

void StrPrint2( char *str2)
		{
		printf ("%s\n",str2);

		 }

		// ok mais la tabulation \n engendre un saut de ligne à l'affichage. cela cré une inconformité entre l'affichage dans le livre et celui de la machine.
        // c'est une erreur fait dans le livre mème.

#############################################################################
#############################################################################
/* 16L08.c......................page 259
ponter vers des fonctions
*/

#include <stdio.h>

     		/* fonction declarartion */

int StrPrint( char *str);

		/* main() fonction */
main() {
		char str[30] = "pointer vers une fonction.";
		int (*ptr)(char *str);
		ptr = StrPrint;
		if ( !(*ptr)(str))
				printf ("termine! \n");

		return 0;
	    }

					/* fonction definition */

int StrPrint( char *str)
		  {

		printf ("%s\n",str);
		return 0;

		  }

		// ok



/*

$ ./a.out
There's music in the sighing of et reed;
There's music in the gushing of a rill;
There's music in all thigns if men had ears;
there earth is but an echo of the spheres.

There's music in the sighing of et reed;
There's music in the gushing of a rill;
There's music in all thigns if men had ears;
there earth is but an echo of the spheres.

*/





#############################################################################
#############################################################################

/*
 
 …*/

/* 17L01.c......................page 264
affectations dynamique de memoire
fontions malloc ();
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

     		/* fonction declarartion */

void StrCopy( char *str1, char *str2);

		/* fonction main() */
main() {
		char str[] = "Affecter de la memoire a l'aide de malloc().";
		char *ptr_str;
		int termination;


				/* appel malloc()  */
		ptr_str= malloc(strlen(str) + 1);
		if ( ptr_str != NULL){

		StrCopy(str, ptr_str);
		printf ("Chaine vers laquelle est dirigee ptr_str :\n %s\n",ptr_str);

		termination=0;
							}
		else{
		printf("echec de malloc ().\n");
		termination=1;
		    }
		return termination;

		}
					/* definition fonction  */


void StrCopy(char *str1, char *str2)
		  {
		 int i;
		for(i=0; str1[i]; i++)
        str2[i]=str1[i];
		str2[i]='\0';


		  }
			//ok

/*
./a.out

Chaine vers laquelle est dirigee ptr_str :
 Affecter de la memoire a l'aide de malloc().


*/






#############################################################################
#############################################################################
/* 17L02.c......................page 266
affectations dynamique de memoire
utilisation conjointe des fontions free () et malloc ();
*/

#include <stdio.h>
#include <stdlib.h>


     		/* declarartion fonction  */

void DataMultiply (int max, int *ptr);
void TablePrint (int max, int *ptr);

		/* main() fonction */
main()
{

            int *ptr_int, max;
            int termination;
            char key ='c';

            max=0;
            termination= 0;

		while (key != 'x')
	{
		printf("entrez un chiffre :\n");
		scanf ("%d", &max);

				/* call malloc()  */
		ptr_int= malloc(max *max *sizeof (int));
		if ( ptr_int != NULL)
	  {

        DataMultiply( max,  ptr_int);
        TablePrint( max, ptr_int);
		free (ptr_int);
	  }

		else
		{
		printf("echec de  malloc().\n");
		termination= 1;
		break;
		}
		printf("\n appuyez sur x pour quitter ; sur une autre touche pour continuer. \n");
		scanf("%s",&key);
	}

		printf("\n A bientot !\n");

		return termination;

}
					/* definition de fonction  */
		void DataMultiply ( int max,  int *ptr)

		   {
		 int i, j;
		for(i=0; i<max; i++)
		for(j=0; j<max; j++)
		*(ptr + i * max + j)= (i+1)*(j+1);
		   }
							/* definition de fonction  */
		void TablePrint ( int max, int *ptr)
			 {
		int i, j;
		printf("table de multiplication de %d :\n", max);
		printf(" ");
		for(i=0; i<max; i++)
				printf("%4d", i+1);
		printf("\n");
		for(i=0; i<max; i++)
		printf("_ _ _ _", i+1);

        for(i=0; i<max; i++){
		printf("\n%d|", i+1);
		for(j=0; j<max; j++)

		printf("%3d ", *(ptr+i *max + j));
				}

			  }
				// ok


#############################################################################
#############################################################################

/* 17L03.c......................page 270
fontions calloc ();
*/

#include <stdio.h>
#include <stdlib.h>


		/* main() fonction */
main()
{
		float *ptr1, *ptr2;
		int i, n;
		int termination = 1;
		n= 5;
		ptr1 = malloc(n * sizeof(float));
		ptr2 = calloc(n,  sizeof(float));

		if ( ptr1 == NULL)
		printf("echec de malloc ().\n");
		else if( ptr2 == NULL)
		printf("echec de calloc ().\n");
		else

	  {
		for(i=0; i<n; i++)
		printf("ptr1[%d]= %5.2f,ptr2[%d]=%5.2f\n", i, *(ptr1 + i), i, *(ptr2 + i));
		free (ptr1);
		free (ptr2);
		termination = 0;

	  }
return termination;

}
	//erreure de fond









#############################################################################
#############################################################################

/* 17L04.c......................page 272
fontions realloc ();
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
			/* declaration de fonction */
void StrCopy( char *str1, char *str2);


		/* main() fonction */
main()
{
	   char *str[4] = {	"There's music in the sighing of a reed;",
				"There's music in the gushing of a rill;",
				"There's music in all thigns if men had ears;",
				"there earth is but an echo of the spheres.\n"
				};
		char *ptr;
		int i;
		int termination = 0;
		ptr = malloc (strlen ((str[0]) + 1) * sizeof(char));
		if ( ptr == NULL)
	{
		printf("echec de malloc ().\n");
		termination=1;
	}
		else
		{
		StrCopy( str[0],ptr);
		printf("%s\n", ptr);

		for(i=1; i<4; i++)
			{
		ptr = realloc (ptr, (strlen (str[i]) + 1) * sizeof(char));
		if( ptr == NULL )
				{
		printf("echec de realloc ().\n");
		termination=1;
		break;
				}
		else
					{
	  	StrCopy( str[i],ptr);
		printf("%s\n", ptr);
					}
			}
		}
		free (ptr);
		return termination;
}
				/* defintion de fonction */
void StrCopy( char *str1, char *str2)

						{
		int i;
		for(i=0; str1[i]; i++)
		str2[i]=str1[i];
		str2[i] = '\0';

						}
						//ok










#############################################################################
#############################################################################

/*
 
 …*/

/* 18L01.c......................page 278
affecter des valeurs aux noms énumérés
definir des   types enum
*/

#include <stdio.h>

		/* main() fonction */
main()
{
	   enum language  {	human = 100,
				animal = 50,
				computer};

		enum days{	SUN,
				MON,
				TUE,
				WED,
				THU,
				FRI,
				SAT};

		printf("humain: %d, animal: %d, ordinateur : %d \n",human, animal, computer);
		printf("SUN: %d\n", SUN);
		printf("MON: %d\n", MON);
		printf("TUE: %d\n", TUE);
		printf("WED: %d\n", WED);
		printf("THU: %d\n", THU);
		printf("FRI: %d\n", FRI);
		printf("SAT: %d\n", SAT);
		return 0;
}

			// OK

/*


*/





#############################################################################
#############################################################################

/* 18L02.c......................page 280
implementer le types enum
utiliser les   types énumérés
*/

#include <stdio.h>

		/* main() fonction */
main()
{
	   enum money_units {penny=1,
						 nickel=5,
						 dime=10,
						 quater=25,
						 dollar =100};
		int money_units[5]= {dollar,
							quater,
							dime,
							nickel,
							penny};
		char *units_name [5] ={"dollar(s)",
							"quater(s)",
							"dime(s)",
							"nickel(s)",
							"penny(s)" };

		int cent, tmp, i;


		printf("entrez un montant exprime en cents \n");
		scanf ("%d", &cent);
		printf("equivalent:\n");
		tmp = 0;
		for (i=0; i<5; i++)
		{
		tmp = cent/ money_units[i];
		cent -= tmp	* money_units[i];
		if(tmp)
		printf("%d %s", tmp, units_name[i]);
		}
		printf("\n");
		return 0;
		}
		// OK








#############################################################################
#############################################################################

/* 18L03.c......................page 283     //ereur page
definition de typedef
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
enum constants{ITEM_NUM = 3,
				  DELT ='a' - 'A'};
	 			  typedef char *STRING[ITEM_NUM];
				  typedef char *PTR_STR;
				  typedef char BIT8;
				  typedef int  BIT16;
void Convert2Upper (PTR_STR str1, PTR_STR str2);

main(){
       STRING str;
	   STRING moon = {"whatever we wear",
                    "we become beautiful",
					  "moon viewing!\n"};

		BIT16 i;
		BIT16 term=0;
		for (i=0; i <ITEM_NUM; i++) {
		str[i] = malloc((strlen(moon[i] )+ 1) * sizeof(BIT8));

		if ( str[i] == NULL) {
		printf("Echec de malloc().\n");
		term = 1;
		break;

}
		Convert2Upper(moon[i], str[i]);
		printf("%s\n",moon[i]);

}
		for(i=0; i <ITEM_NUM; i++)
	{
		printf("\n%s",str[i]);
		free (str [i]);
	}
			return term;
			}

					/* defintion de fonction */

void Convert2Upper(PTR_STR str1, PTR_STR str2)
{
		BIT16 i;
		for(i=0; str1[i]; i++) 
		{
		if (( str1[i] >= 'a') &&  (str1[i] <= 'z'))
		str2[i] = str1[i]- DELT;
		else

		str2[i]=str1[i];
		}
		str2[i]='\0';

		}
		  // ok

#############################################################################
#############################################################################
/* 18L04.c............................page 285
Definition de Type
syntaxe

*/


# include <stdio.h>


enum con {MIN_NUM = 0, MAX_NUM = 100};
		int fRecur (int n);
main ()
 {

		int i, sum1, sum2;
		sum1 = sum2= 0;
		for (i=1; i<=MAX_NUM; i++)
		sum1+=i;

		printf ("Valeur de sum1: %d.\n", sum1);
		sum2 = fRecur(MAX_NUM);

		printf ("Valeur renvoyée par fRecur(): %d.\n", sum2);

		return 0;
		}
int fRecur(int n)
		{

		if (n == MIN_NUM)
		return 0;
		return fRecur(n-1) + n;
		}
       // OK
#############################################################################
#############################################################################

/* 18L05.c............................page 288
Reception des arguments de ligne de commande
syntaxe

*/


# include <stdio.h>

main (int argc, char *argv[], char *en[])
 {

		int i;

		printf ("Valeur recue de argc   : %d\n", argc);// LE "de" aurait du etre remplacé par "par".
		                                               //Mais cette erreur provient du livre mème.

		printf ("Nombre d'arguments passees à la fonction main()    : %d.\n", argc);

		printf ("Premier argument : %s\n", argv[0]);
				printf ("Autres arguments :\n");
		for (i=1; i<argc; i++)
		printf (" %s", argv[i]);
		printf("\n");
return 0;

		}
		//ok




#############################################################################
#############################################################################

/*
 
 …*/

/* 19L01.c............................page 297
Lecture des membres d'une structure
Syntaxe
struct struct_tag {
		type1 variable1;
		type2 variable2;
		type3 variable3;
		.
		.
		.
		}
*/

# include <stdio.h>

main(void)
 {

		struct computer {
		float cost;
		int year;
		int cpu_speed;
		char cpu_type[16];
		} model;


		printf ("Type du processeur de votre ordinateur ?\n");
		gets (model.cpu_type);


		printf ("Vitesse du processeur (en MHZ) ?\n");
		scanf ("%d", &model.cpu_speed);

		printf ("Annee de construction ?\n");
		scanf("%d", &model.year);
		printf("prix d'achat ?\n");
		scanf("%f",&model.cost);

				printf ("Informations saisies :\n");

		printf("Annee   : %d\n", model.year);
		printf("prix    : %6.2f\n",model.cost);
		printf("Type CPU: %s\n", model.cpu_type);
		printf("Vitesse : %d MHZ\n", model.cpu_speed);


return 0;

		}
   //ok corect, mais  il faut injecter a la saisie, le symbol dolard
   //qui n'apparait pas dans le code du livre afin que les resultats soient conforme. .




#############################################################################
#############################################################################

/* 19L02.c............................page 299
Initialisation d'une structure
Syntaxe

*/

# include <stdio.h>

main (void)
 {

		struct employee {

		int id;
		char name[32];
		};
		struct employee info = {
		0001,
		"B. Smith"
		};


		printf ("Echantillon    : \n");
				printf ("Nom du salarie : %s\n", info.name);
		printf ("Reference      : %04\n", info.id);


		printf ("Tapez votre nom    : \n");
				gets (info.name);


		printf ("Tapez votre Reference  : \n");
		scanf("%d" , &info.id);

				printf ("Informations saisies   :\n");



		printf ("Nom        : %s\n", info.name);
		printf("Reference: %04d\n", info.id);

return 0;

		}
		//  tout semble correct sauf que la refference 0001 n'apparait pas.




#############################################################################
#############################################################################


/* 19L03.c............................page 301
Transmission d'une structure à une fonction
Syntaxe

*/

# include <stdio.h>


		struct computer {
		float cost;
		int year;
		int cpu_speed;
		char cpu_type[16];
		};
		typedef struct computer SC;
		SC DataReceive(SC s);


main (void)
 {
		SC model;


		model = DataReceive (model);

		printf ("Informations saisies   :\n");
		printf ("Annee  :   %d\n", model.year);
		printf("prix    :   %6.2f\n",model.cost);
		printf ("Type   :   %s\n", model.cpu_type);
		printf ("Vitesse: %d MHZ\n", model.cpu_speed);

return 0;

		}
		SC DataReceive(SC s)
		 {


		printf ("Type du processeur de votre ordinateur ?\n");
		gets (s.cpu_type);


		printf ("Vitesse (exprimee en MHZ) ?\n");
		scanf ("%d", &s.cpu_speed);

		printf ("Annee de construction ?\n");
		scanf("%d", &s.year);
		printf("prix d'achat ?\n");
		scanf("%f",&s.cost);

return s;
		  }

		  // ok




#############################################################################
#############################################################################
/* 19L04.c............................page 303
Passer un pointeur dirigé vers une structure
Syntaxe

*/

# include <stdio.h>


		struct computer {
		float cost;
		int year;
		int cpu_speed;
		int cpu_type[16];
		};
		typedef struct computer SC;
		void DataReceive(SC *ptr_s);


main (void){
		SC model;


		DataReceive (&model);

		printf ("Informations saisies   :\n");
		printf ("Annee  : %d\n", model.year);
		printf("prix    : %6.2f\n", model.cost);
		printf ("Type   : %s.\n", model.cpu_type);
		printf ("Vitesse: %d MHZ\n", model.cpu_speed);

return 0;

		}
		void DataReceive(SC *ptr_s)
		 {


		printf ("Type du processeur de votre ordinateur ?\n");
		gets ((*ptr_s).cpu_type);


		printf ("Vitesse en MHZ ?\n");
		scanf ("%d", &(*ptr_s).cpu_speed);

		printf ("Annee de construction ?\n");
		scanf("%d", &(*ptr_s).year);
		printf("prix d'achat ?\n");
		scanf("%f",&(*ptr_s).cost);

			}
         // ok





#############################################################################
#############################################################################



//==============================================
NOTA:

L'operateur  (->)

Il designe un membre de la structure vers lequel  est dirige un pointeur.

EX:  ( 1)    peut s'ecrire sous forme de (2)

(*ptr_s).cpu_type		(1)

ptr_s -> cpu_type		(2) 




et (3) peut s'ecrire sous forme de (4)

&(*ptr_s).cpu_speed		(3)

&(ptr_s->cpu_speed)		(4)



L'operateur ->  est plus utilisé que l'oprateur .    , car il simplifie la syntaxe des programmes


//==============================================/


/* 19L05.c............................page 306
Tableaux de structure
Syntaxe

*/

# include <stdio.h>


		struct haiku {
        int start_year;
        int end_year;
char author [16];
char str1[32] ;
char str2[32] ;
char str3[32] ;

		};
		typedef struct haiku HK;
		void DataDisplay(HK *ptr_s);


main (void)
 {
		HK poem[2] = {
				{1641, 1716, "Sodo", "Leading me along", "my shadow goes back home", "from loocking at the moon."},
				{1729, 1781, "Chora", "A storm wind blows", "out from among the grasses", "the full moon grows."}

					  };
		int i;
		for (i=0; i<2; i++)

		DataDisplay (&poem[i]);

return 0;

		}
		void DataDisplay(HK *ptr_s)
		 {


		printf ("%s\n", ptr_s -> str1);



		printf ("%s\n", ptr_s -> str2);

		printf ("%s\n", ptr_s -> str3);
		printf("---%s\n",ptr_s -> author);
		printf ("    (%d - %d)\n\n", ptr_s -> start_year, ptr_s -> end_year);



			}

        //ok

#############################################################################
#############################################################################


/* 19L06.c............................page 308
Structures imbriquées
Syntaxe

*/

# include <stdio.h>


		struct department {
		int code;
		char name[32];
		char position [16];
		};
		typedef struct department DPT;
		struct employee {
					DPT d;
					int id;
					char name[32];
                };
        typedef struct employee EMPLY;

		void InfoDisplay(EMPLY *ptr);
		void InfoEnter (EMPLY *ptr);

main (void){
    EMPLY info = {
				{01, "Marketing", "Directeur"},
				0001,
				"B. Smith"};
	printf("Exemple : \n");
		InfoDisplay(&info);
		InfoEnter (&info);

		printf ("\nInformations saisies: \n");
		InfoDisplay(&info);


return 0;
}


		void InfoDisplay(EMPLY *ptr){

		printf("Nom: %s\n",ptr -> name);
		printf("Reference   : %04d\n", ptr ->id);
		printf("Nom service : %s\n", ptr -> d.name);
		printf("code service: %02d\n", ptr -> d.code);
		printf("Poste : %s\n",ptr -> d.position);
		}
		void InfoEnter (EMPLY *ptr){

		printf ("\nEntrez vos informations: \n");
		printf("Nom: \n");
		gets ((*ptr).name);
		printf("Poste: \n");
		gets ((*ptr).d.position);
		printf("Nom service: \n");
		gets ((*ptr).d.name);
		printf("code service : \n");
		scanf ("%d", &(ptr-> d.code));
		printf("Reference : \n");
		scanf ("%d", &(ptr-> id));


			}
   // ok




#############################################################################
#############################################################################
/*
 
 …*/

/* 20L01.c............................page 319
references aux membres d'une union.
Structure recurrente
Syntaxe

*/

# include <stdio.h>
# include <string.h>


main (void)
 {
		union menu {
				char name[23];
				double price;
						} dish;


		printf("Affectations separees de valeurs : \n");

				/* acceès au nom */
		strcpy (dish.name, "Kouign Amann");

		printf("Gateau : %s\n", dish.name);
		/* access to price */
		dish.price = 12.50;
		printf("prix : %5.2f\n", dish.price);

		return 0;





			}

			// ok





#############################################################################
#############################################################################


/* 20L02.c............................page 322
partage de la memoire
Syntaxe

*/

# include <stdio.h>
# include <string.h>


main (void)
 {
		union employee  {
				int start_year;
				int dpt_code;
				int id_number;
						} info;

					/* initialisation de start_year */
		info.start_year = 1997;

					/* initialisation de dpt_code */
		info.dpt_code = 8;

					/* initialisation de id_number */
		info.id_number = 1234;

					/* affichage du contenu de l'union */

		printf("Annee : %d\n", info.start_year);
		printf("Code service    : %d\n", info.dpt_code);
		printf("Identificateur  : %d\n", info.id_number);

		return 0;

			}

			//ok


#############################################################################
#############################################################################

/* 20L03.c............................page 323
Calculer la taille d'une  union
Syntaxe
*/

# include <stdio.h>
# include <string.h>


main (void)
 {
		union u {
				double x;
				int y;
						} a_union;
		struct s {
		double x;
		int y;
				} a_struct;



		printf("Taille de double  : %d-octets\n", sizeof (double));
		printf("Taille de int : %d-octets\n", sizeof (int));


		printf("Taille de a_union : %d-octets\n",  sizeof(a_union));
		printf("Taille de a_struct : %d-octets\n",  sizeof(a_struct));

		return 0;

			}
       //ok

#############################################################################
#############################################################################

/* 20L04.c............................page 325
Reference d'un meme emplacement
Syntaxe
*/

# include <stdio.h>


union u {
				char ch [2];
				int num;
						};
int UnionInitialize (union u  val);

main (void)
 {
		union u val;

		int x;
		x = UnionInitialize (val);



		printf("Constantes caractere de l'union : \n ");
		printf("%c\n ", x & 0x00FF);	//obtension de l'octet inférieur
		printf("%c\n ", x >> 8);	 // l'octet superieur est obtenu par le decalage de 8 bits vers la droite'

return 0;
}

int UnionInitialize (union u  val)

		{
		val.ch[0] = 'H';
		val.ch[1] = 'i';

		return val.num;

			}
    // ok





#############################################################################
#############################################################################
/* 20L05.c............................page 327
Imbrication d'une structure dans une union
Syntaxe
*/

# include <stdio.h>
# include <string.h>

struct survey {
				char name [20];
				char c_d_p;
				int age ;
				int hour_per_week;
				union {
				char cable_company[16];
				char dish_company[16];
					} provider;
				};
void DataEnter (struct survey *s);
void DataDisplay (struct survey *s);

main (void)
 {
		struct survey tv ;

		DataEnter (&tv);
		DataDisplay (&tv);
return 0;
	}
					/*defintion de focntion  */
void DataEnter (struct survey *ptr){
		char is_yes[4];
		printf("Etes vous abonne au cable ? (Oui ou Non) \n");
		gets (is_yes);
		if ((is_yes [0] == 'O')|| (is_yes[0] == 'o')) {
		printf("Tapez le nom du fournisseur d'acces :\n");
		gets (ptr -> provider.cable_company);
		ptr ->	c_d_p = 'c';										}
		else {
		printf("Etes vous abonne a un bouquet satellite ? (Oui ou Non) \n");
		gets (is_yes);
		if (( is_yes [0] == 'O') || (is_yes[0] == 'o')){
		printf("Tapez le nom du fournisseur d'acces :\n");
		gets (ptr -> provider.dish_company);
		ptr ->	c_d_p = 'd';						}

		else {
		ptr ->c_d_p = 'p';
		  }

        }

		printf("Tapez votre nom : \n");
		gets (ptr->name);
		printf("Votre age: \n");
		scanf("%d", &ptr->age );
		printf("Combien d'heures par semaine regardez vous la television ? \n");
		scanf("%d", &ptr->hour_per_week );
		}

				/*defintion de focntion  */
void DataDisplay (struct survey *ptr)
	{

		printf("\n Information saisie : \n ");
		printf("Non: %s\n ", ptr-> name);
		printf("Age: %d \n ",ptr-> age);
		printf("Nombre d'heure : %d\n",ptr-> hour_per_week );

		if (ptr->c_d_p == 'c')
		printf("Fournisseur d'acces au cable : %s\n",ptr-> provider.cable_company);

		else if  (ptr->c_d_p == 'd')
		printf("Fournisseur d'acces au  satellite: %s \n", ptr-> provider.dish_company);
        else
		printf("Vous n'etes abonne ni au cable ni au satellite.\n");
		printf("\n Merci et a bientot \n");

		}

		//okk


#############################################################################
#############################################################################
/*
 
 …*/
/* 21L01.c............................page 341
Ouverture et fermetture d'un fichier
Syntaxe

Ouverture de fichier
# include <stdio.h>
FILE *fopen(const char *nom_fichier, const char *mode);

fermer un fichier
int fclose (FILE *flux);
*/

#include <stdio.h>

enum {SUCCESS, FAIL};

main (void){
		FILE *fptr;
		char filename[]= "haiku.txt";
		int reval = SUCCESS;

	if ((fptr = fopen(filename, "r")) == NULL){
		printf("Ouverture de %s impossible.\n", filename);
		reval = FAIL;
		 } 
	else {	printf("valeur de fptr : 0x%p\n", fptr);
		
		fclose (fptr);
		}
		return reval;
}
            // OK





#############################################################################
#############################################################################


	/* 21L02.c............................page 343
	Lecure et écriture par caractère
	Syntaxe

	# include <stdio.h>
	int  fgetc(FILE *flux);

	int fputc (int c, FILE *flux);
	*/

	# include <stdio.h>

	enum {SUCCESS, FAIL};
	void CharReadWrite(FILE *fin, FILE *fout);

	main (void)
	 {
			FILE *fptr1, *fptr2;
			char filename1[]= "outhaiku.txt";
			char filename2[]= "haiku.txt";
			int reval = SUCCESS;

			if ((fptr1 = fopen(filename1, "w")) == NULL) {
			printf("Ouverture de %s impossible.\n", filename1);
			reval= FAIL;
												}
			else if ((fptr2 = fopen (filename2, "r")) == NULL){
			printf("Ouverture de %s impossible.\n", filename2);
			reval = FAIL;
			}
			else {
			CharReadWrite(fptr2, fptr1);
			fclose(fptr1);
			fclose(fptr2);

			}
			return reval;
		}
									/* Defintion de fonction */
			void CharReadWrite (FILE *fin, FILE *fout)
			{
			int c;
			while ((c=fgetc(fin)) != EOF) {
			putchar(c);
			fputc(c, fout);
			}
        }
                // pas d'erreurs de structure. faille non decélé.




#############################################################################
#############################################################################


	/* 21L03.c............................page 346
	Lecure-écriture par ligne
	Syntaxe

	# include <stdio.h>
				fgets
	char  *fgetc(char *s, int n, FILE *flux);
				fputs
	int fputs (const char *s, FILE *flux);
	*/

	# include <stdio.h>

	enum {SUCCESS, FAIL, MAX_LEN = 81};
	void LineReadWrite(FILE *fin, FILE *fout);

main (void){
FILE *fptr1, *fptr2;
char filename1[] = "outhaiku.txt";
char filename2[] = "haiku.txt";
int reval = SUCCESS;

if ((fptr1 = fopen (filename1, "w")) == NULL) {
			printf("ecriture impossible dans %s.\n", filename1);
			reval= FAIL;
														}
			else if ((fptr2 = fopen (filename2, "r")) == NULL){
			printf("Ouverture impossible de %s .\n", filename2);
			reval = FAIL;
															}
			else {
			LineReadWrite(fptr2, fptr1);
			fclose (fptr1);
			fclose (fptr2);

				}
			return reval;
		}
									/* Defintion de fonction */
void LineReadWrite (FILE *fin, FILE *fout){
			char buff [MAX_LEN];
			while (!feof (fin)) {
			fgets(buff, MAX_LEN, fin);
			printf("%s", buff);
			fputs (buff, fout);}
			}
                //pas d'erreurs de structure. faille non decélé.




#############################################################################
#############################################################################


	/* 21L04.c............................page 349
	Lecure et écriture par bloc
	Syntaxe

	# include <stdio.h>
				read
	size_t  fread( void *ptr *s, size_t n, FILE *flux);
				write
	size_t fwrite (const void *ptr, size_t taille, FILE *flux);
	*/

	# include <stdio.h>

	enum {SUCCESS, FAIL, MAX_LEN = 80};
	void BlocReadWrite (FILE *fin, FILE *fout);
	int ErrorMsg (char *str);

	main (void)
	 {
			FILE *fptr1, *fptr2;
			char filename1[]= "outhaiku.txt";
			char filename2[]= "haiku.txt";
			int reval = SUCCESS;

			if ((fptr1 = fopen (filename1, "w")) == NULL) {
			reval= ErrorMsg (filename1);
														}
			else if ((fptr2 = fopen (filename2, "r")) == NULL){
			reval = ErrorMsg (filename2);
															}
			else {
			BlocReadWrite (fptr2, fptr1);
			fclose (fptr1);
			fclose (fptr2);

				}
			return reval;
		}
			void BlocReadWrite (FILE *fin, FILE *fout)
			{
			int num;
			char buff [MAX_LEN + 1];

			while (!feof (fin)) {
			num = fread(buff, sizeof(char), MAX_LEN, fin);
			buff[num *sizeof(char)] = '\0';
			printf("%s", buff);
			fwrite(buff, sizeof (char), num, fout);
								}
			}
									/* Defintion de fonction */
			int ErrorMsg(char *str)
			{
			printf("Ouverture impossible de %s.\n", str);
			return FAIL;
			}
              //pas d'erreurs de structure. faille non decélé.

#############################################################################
#############################################################################
/*
 
 …*/
	/* 22L01.c............................page 357
	Accès aléatoire à un fichier
	Syntaxe

	# include <stdio.h>
				fseek
	int  fseek( FILE *flux, long decalage, int valeur );
				ftell
	long  ftell ( FILE *flux);
	*/

	# include <stdio.h>

	enum {SUCCESS, FAIL, MAX_LEN = 80};
	void PtrSeek (FILE *fptr);
	long PtrTell (FILE *fptr);
	void DataRead (FILE *fptr);
	int ErrorMsg (char *str);

main (void){
			FILE *fptr;
			char filename [] = "haiku.txt";
			int reval = SUCCESS;

			if ((fptr = fopen (filename, "r")) == NULL) {
			reval= ErrorMsg (filename);	// use of : int ErrorMsg(char *str);
														}

			else {
			PtrSeek (fptr); 	// use of : void PtrSeek(FILE *fptr);
			fclose (fptr);

				}
			return reval;
		}
											/* Defintion de fonction */

			void PtrSeek(FILE *fptr)
			{
			long offset1, offset2, offset3;
			offset1 = PtrTell (fptr);
			DataRead (fptr);
			offset2 = PtrTell (fptr);
				/* Aller au troisième vers du poème  */
			fseek(fptr, 26L, SEEK_CUR);
			offset3 = PtrTell (fptr);
			DataRead (fptr);
			printf("\nNouvelle lecture du poeme\n");
				/* Relir le troisième vers  */
			fseek(fptr, offset3, SEEK_SET);
			DataRead (fptr);
				/* Relir le deuxième vers  */
			fseek(fptr, offset2, SEEK_SET);
			DataRead (fptr);
				/* Relir le premier vers  */
			fseek(fptr, offset1, SEEK_SET);
			DataRead (fptr);
							}

					/* Defintion de fonction */

			long PtrTell (FILE *fptr)

			 {
			 long reval;
			reval = ftell(fptr);
			printf("fptr est sur %ld", reval);
			return reval;
				}
				/* Defintion de fonction */

			void DataRead (FILE *fptr)
				{
			char buff[MAX_LEN];
			printf("--- %s", buff);
					}

				/* Defintion de fonction */


			int ErrorMsg(char *str)
			{
			printf("Ouverture impossible de %s.\n", str);
			return FAIL;
				}
// PAS D'ERREURES DE STRUCTURE





#############################################################################
#############################################################################
/* 22L02.c............................page 361
	Lecture et ecriture de données binaires
	Syntaxe

	# include <stdio.h>
				fonction rewind ()
	void rewind( FILE *flux);
	*/

	# include <stdio.h>

	enum {SUCCESS, FAIL, MAX_NUM = 3};
	void DataWrite (FILE *fout);
	void DataRead (FILE *fptr);
	int ErrorMsg (char *str);

	main (void)
	 {
			FILE *fptr;
			char filename [] = "double.bin";
			int reval = SUCCESS;

			if ((fptr = fopen (filename, "wb+")) == NULL) {
			reval= ErrorMsg (filename);
														}

			else {
			DataWrite (fptr);
			rewind (fptr);
			DataRead (fptr);
			fclose (fptr);

				}
			return reval;
		}
											/* Defintion de fonction */

			void DataWrite(FILE *fout)
			{
			int i;
			double buff[MAX_NUM] ={
								123.45,
								567.89,
								100.1};

			printf("Taille de buff: %d octets\n", sizeof(buff));
			for (i=0; i < MAX_NUM; i++)
			printf(" %5.f\n", buff[i]);
			fwrite (&buff[i], sizeof(double), 1, fout);
			}

					/* Defintion de fonction */

void DataRead (FILE *fin)
		{
			 int  i;
			double x;
			printf("Lecture du fichier binaire:\n");
			for (i=0; i < MAX_NUM; i++){
			fread(&x, sizeof(double), (size_t)1, fin);
			printf(" %5.f\n", x);

			}
				}


				/* Defintion de fonction */


			int ErrorMsg(char *str)
			{
			printf("Ouverture impossible de %s.\n", str);
			return FAIL;
				}
// PAS D'ERREURES DE STRUCTURES
#############################################################################
#############################################################################
/* 22_L03.c....implementation des fonctions fscanf()
et fprintf().page 364
*/
    # include <stdio.h>

	enum {SUCCESS, FAIL, MAX_NUM = 3,STR_LEN=23};

	void DataRead (FILE *fin);
	int ErrorMsg (char *str);
	main (void)
	 {
			FILE *fptr;

	char filename [] = "strnum.mix";
	int reval = SUCCESS;


	if ((fptr = fopen (filename, "w+")) == NULL) {
		reval= ErrorMsg (filename);}

		else {
			DataWrite (fptr);
			rewind (fptr);
			DataRead (fptr);
			fclose (fptr);
            }

		return reval;
		}
	/* fonction */

	void DataWrite(FILE *fout)
			{
			int i;
	char cities[MAX_NUM][STR_LEN] = {
	            "St.Louis->Houston :",
	            "Houston->Dallas :",
                "Dallas ->philadelphia :"};

	int miles[MAX_NUM] = { 845,243,1459};
	printf("Donnees ecrites :\n");
	for(i=0; i < MAX_NUM; i++){
    printf("%-23s  %d\n",cities[i],miles[i]);
    fprintf(fout, "%s  %d\n", cities[i],miles[i]);
	}
	}
		/* fonction */

	void DataRead(FILE *fin) {
    int i;
	int miles;
	char cities[STR_LEN];

	printf("\nDonnees lues:\n");
	for(i=0; i < MAX_NUM; i++){
	fscanf(fin, "%s%d",cities, &miles);
	printf("%-23s  %d\n, cities, miles") ;
	}
	}

     /* fonction */
	int ErrorMsg(char *str)
	{
	printf("Ouverture de %s impossible.\n",str);

	 return FAIL;

	}

#############################################################################
#############################################################################
	/* 22L04.c............................page 366
	Rediriger un  flux standard
	Syntaxe

	# include <stdio.h>
				fonction fcanf ()
	FILE *freopen(const char *fichier, const char *mode, FILE *flux);
	*/
	# include <stdio.h>

	enum {SUCCESS, FAIL, STR_NUM = 4};
	void StrPrint(char **str);
	int ErrorMsg (char *str);

	main (void)
	 {
			char  *str[STR_NUM] = {
							"Be bent, and you will remain straight.",
							"Be vacant, and you will remain full.",
							"Be worn, and you will remain new."
							"------LaoTzu"};
			char filename [] = "LaoTzu.txt";
			int reval = SUCCESS;
			StrPrint(str);

			if (freopen (filename, "w", stdout) == NULL) {
			reval= ErrorMsg (filename);
														}

			else {
			StrPrint (str);
			fclose (stdout);

				}
			return reval;
		}
											/* Defintion de fonction */

			void StrPrint(char **str)
			{
			int i;
			for(i=0; i < STR_NUM; i++)
			printf(" %s\n", str[i]);
			}

				/*  fonction */

			int ErrorMsg(char *str)
			{
			printf("Ouverture impossible de %s.\n", str);
			return FAIL;
				}
// PAS D'ERREURES DE STRUCTURES

#############################################################################
#############################################################################
/*
 
 …*/
	/* 23L01.c............................page 374
	Directive #define
	Syntaxe

	# include <stdio.h>
				fonction fcanf ()
	FILE *freopen(const char *fichier, const char *mode, FILE *flux);
	*/

	# include <stdio.h>

	#define METHOD "ABS"
	#define ABS(val) 	((val) < 0 ? -(val) : ( val))
	#define MAX_LEN	8
	#define NEGATIVE_NUM -10

	main (void)
	 {
			char  *str = METHOD;
			int array [MAX_LEN];
			int i;
			printf(" Valeur d'origine du tableaux:\n");
			for(i=0; i < MAX_LEN; i++) {
			array [i] = (i+1) * NEGATIVE_NUM;
			printf("array [%d]: %d\n", array[i]);
										}
			printf("\n Macro %s:\n", str);
			for(i=0; i < MAX_LEN; i++){
			printf("ABS(%d): %3d\n", array[i], ABS(array[i]));
									}

			return 0;
				}

                      // PAS D'ERREURES DE STRUCTURES




#############################################################################
#############################################################################

	/* 23L02.c............................page 378
	Implémentation des directives #fdef #Ifndef et #endif
	Syntaxe

	# include <stdio.h>
				fonction fcanf ()
	FILE *freopen(const char *fichier, const char *mode, FILE *flux);
	*/

	# include <stdio.h>

	#define UPER_CASE 0
	#define NO_ERROR 0


	main (void)
	 {
			#ifdef UPER_CASE
			printf(" LIGNE AFFICHEE,\n");
			printf(" CAR UPER_CASE EST DEFINIE.\n");

			#endif
			#ifndef	LOWER_CASE
			printf(" \n Ligne  affichee,\n");
			printf(" CAR LOWER_CASE n'est pas definie.\n");
			#endif


			return NO_ERROR;
				}
            // ok

#############################################################################
#############################################################################
	/* 22L01.c:..Directives #if, #elif, #else.............page 381
	#ifdef, #elif, et #else.*/

    #include <stdio.h>

	#define C_LANG  'C'
	#define B_LANG   'B'
	#define NO_ERROR  0

	main (void)
	 {
    #if C_LANG == 'C' && B_LANG == 'B'
        #undef C_LANG
        #define C_LANG "Je connais le C.\n"
        #undef B_LANG
        #define B_LANG "et le basique.\n"
        printf("%s%s", C_LANG, B_LANG);

    #elif C_LANG == 'C'
        #undef C_LANG
        #define C_LANG "Je connais uniquement le C.\n"
        printf("%s", C_LANG);
    #elif B_LANG == 'B'
        #undef B_LANG
        #define B_LANG "Je connais uniquement le basique.\n"
        printf("%s", B_LANG);
    #else
        printf("Je ne  connais ni le C ni le basique.\n");
    #endif
        return NO_ERROR;
	 }

     /*problème curieux: Problème de compilation sur code block,
      mais pas de problème sur gedit. De plus,d'autres anomalies
       apparaissent encore sur code block. Il sagit de certaines lignes
        qui apparaissent soulignées.  */




#############################################################################
#############################################################################

	/* 23L04.c............................page 383
	Directive #if imbiquées
	Syntaxe

	*/

	# include <stdio.h>

			/* definitions de macro */

	#define ZERO 0
	#define ONE 1
	#define TWO (ONE +ONE)
	#define THREE (ONE + TWO)
	#define TEST_1 ONE
	#define TEST_2 TWO
	#define TEST_3 THREE
	#define MAX_NUM THREE
	#define NO_ERROR ZERO
				/* fonction */
	void StrPrint(char **ptr_s, int max);

				/* fonction main () */

	main (void)
	 {

	 char *str[MAX_NUM] = {"The choice of a point of view"
						"if the initial act of culture.",
						"--- d'apres 0. Gasset"};
		#if TEST_1 ==1
		#if TEST_2 == 2
		#if TEST_3 == 3
		StrPrint(str, MAX_NUM);
		#else
		StrPrint(str, MAX_NUM-ONE);
		#endif
		#else
		StrPrint(str, MAX_NUM-TWO);
		#endif
		printf(" Aucune macro de test definie.\n");
		#endif
		return NO_ERROR;
		}
						/* fonction */
	void StrPrint(char **ptr_s, int max)
	{
		int i;
		for(i=0; i<max; i++)
		printf(" Adresse: 0x%p\n Contenu:%s .\n", *ptr_s[i], ptr_s[i]);

				}
#############################################################################
#############################################################################










#############################################################################
#############################################################################








