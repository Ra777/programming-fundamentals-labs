/*
From Zhang 


*/


﻿
/*
pp 36-35

*/

#include <stdio.h>

// addition de 2 entier etrenvoi du resultat


int integer_add(int x, int y)
{
int result;
result= x+y;
 return result;
}

//Noter que la fct integer_add  est placée avant le main
int main(){
	int sum;
	sum=integer_add(5, 12);
	printf("5+12= donne %d.\n", sum);
	return 0;
	}


#############################################################################
#############################################################################

﻿/*
04L04.c    , p50

*/



#include <stdio.h>

main(){

	int int_num1, int_num2, int_num3;     // declaration de variable entieres


	float flt_num1, flt_num2, flt_num3;    // declaration de variable virgule flotante

	int_num1=32/10;                // diviseur et dividendde entier
	flt_num1=32/10;
	int_num2=32.0/10;              // diviseur  entier
	flt_num2=32.0/10;
	int_num3=32/10.0;               //  dividendde entier
	flt_num3=32/10.0;

	printf("la division entiere de 32 par 10=%d.\n", int_num1);
	printf("la division decimale de 32 par 10=%f.\n", flt_num1);
	printf("la division entiere de32.0par 10=%d.\n", int_num2);
	printf("la division decimale de 32.0 par 10=%f.\n", flt_num2);
	printf("la division entiere de32.0par 10.0=%d.\n", int_num3);
	printf("la division decimale de 32 par 10.0=%f.\n", flt_num3);

	return 0;
	}



#############################################################################
#############################################################################

﻿

/* prog qui lit le caractère entré au clavier ar l'utilisateur et l' affiche à l'ecran.



*/
// 05L01.c ....p59

#include <stdio.h>
// lecture d'un caractere saisi au clavier
main(){
int ch;
printf("tapez un caractere:\n");
ch = getc(stdin);
printf("caractere saisi est:%c\n", ch);
return 0;}



#############################################################################
#############################################################################



//05L06.c.....p67


#include <stdio.h>

main(){
/* afficher la longueur minimal de champ*/


	int num1, num2;
 	num1=12;
	num2=12345;
printf("affichage de la lonngueur minimal de champ\n");
printf("%d\n",num1);
printf("%5d\n",num2);
printf("%5d\n",num1);
printf("%05d\n",num1);
printf("%2d\n",num2);
return 0;
}

#############################################################################
#############################################################################

﻿//05L03.c.....p62// affichage d'un caractère a l'aide de putc




#include <stdio.h>

main(){

	int ch;

	ch=65; // valeur numerique de A

	printf(" caratere correspondant au code ASCII 65:\n");
	putc(ch, stdout   );
	printf("\n");
	return 0;}



#############################################################################
#############################################################################
//05L04.c.....p63 affichage de caractere à l'aide de putchar


#include <stdio.h>

main(){
	
	putchar(65);
	putchar(10);
	putchar(66);
	putchar(10);
	putchar(67);
	putchar(10);
	int ch;
	
	return 0;
	}
/*
./exe
A
B
C*/




#############################################################################
#############################################################################

﻿/* 05l05 page66

synthase: int printf(const char* format,...)
syntaxe de cours de cours: fonction printf()
#include <stdio.h>
int printf(constchar*format,.expression.);

*/
//

#include <stdio.h>
// lecture d'un caractere saisi au clavier
main(){


	printf("affichage des nombres en Hexadecimal majuscule, en hexadecimal miniscule puis en decimal\n");
	printf("Hex maj  Hex min decimal\n");
	printf("% X      % x     % d \n", 0,0,0);
	printf("% X      % x     % d \n", 1,1,1);
	printf("% X      % x     % d \n", 2,2,2);
	printf("% X      % x     % d \n", 3,3,3);
	printf("% X      % x     % d \n", 4,4,4);
	printf("% X      % x     % d \n", 5,5,5);
	printf("% X      % x     % d \n", 6,6,6);
	printf("% X      % x     % d \n", 7,7,7);
	printf("% X      % x     % d \n", 8,8,8);
	printf("% X      % x     % d \n", 9,9,9);
	printf("% X      % x     % d \n", 10,10,10);
	printf("% X      % x     % d \n", 11,11,11);
	printf("% X      % x     % d \n", 12,12,12);
	printf("% X      % x     % d \n", 13,13,13);
	printf("% X      % x     % d \n", 14,14,14);
	printf("% X      % x     % d \n", 15,15,15);

return 0;
}




#############################################################################
#############################################################################


//05L06.c.....p67


#include <stdio.h>

main(){
/* afficher la longueur minimal de champ*/


	int num1, num2;
 	num1=12;
	num2=12345;
printf("affichage de la lonngueur minimal de champ\n");
printf("%d\n",num1);
printf("%5d\n",num2);
printf("%5d\n",num1);
printf("%05d\n",num1);
printf("%2d\n",num2);
return 0;
}



#############################################################################
#############################################################################

﻿//05L07.c.....p66

#include <stdio.h>

main(){
// justification a droite et justification a gauche


	int num1, num2, num3,num4,num5;
	num1=1;
	num2=12;
	num3=123;
	num4=1234;
	num4=12345;
	num2=12345;
	printf("affiche les nombres: alignes a droites et a gauche\n");

	printf("%8d %-8d\n",num1,num1);
	printf("%8d %-8d\n",num2,num2);
	printf("%8d %-8d\n",num3,num3);
	printf("%8d %-8d\n",num4,num4);
	printf("%8d %-8d\n",num5,num5);
return 0;
}




#############################################################################
#############################################################################

﻿//05L08.c.....p70


#include <stdio.h>

main(){
/*  indicateur de precision */


	int int_num;
	double flt_num;
 	int num1=123;
	double flt_numn=123.456789;
printf("format entier par defaut:		%d\n", int_num);
printf(" avec indicateur de precision:	%2.8d\n",int_num);
printf("format decimal par defaut:		%f\n",flt_numn);
printf("avec indicateur de precision:	%-10.2f\n",flt_numn);
printf("%05d\n",num1);

return 0;
}




#############################################################################
#############################################################################



﻿/*6L01.c.....p66

syntaxe de cours:operande gauche= operande droit()
#include <stdio.h>
operande gauche= operande droit);
*/




#include <stdio.h>

main(){
// operateur arithmetique d'affection


	int x,y,z;
	x=1;
	y=3;
	z=10;

	printf("comme  x=%d, y=%d et z=%d,\n", x,y,z);

	x=x+y;
	printf("x=x+y affecte %d a x;\n", x);

	x=1;
	x+=y
    printf(" x += y affecte %d a x;\n", x);

	x=1;
	z=z*x+y;
	printf("z =z*x+y affecte %d a z;\n", z);

	z=10;
	z=z*(x+y);
	printf("z=z*(x+y) affecte %d a z;\n", z);

	z=10;
	z *= x+y;
	printf("z*=x+y affecte %d a z;\n", z);

return 0;
}



#############################################################################
#############################################################################


//06L02 p83


#include <stdio.h>

main(){
// longueur minimal de champ


int w,x,y,z, result;

	w=x=y=z=1;  // on a initialise a 1

	printf("comme  w=%d, x=%d, y=%d et z=%d,\n", w,x,y,z);

	result= ++w;

	printf("++w donne: %d,\n", result);

	result= x++;

	printf("x++ donne: %d,\n", result);
	
	result= --y;
	
	printf("--y donne: %d,\n", result);
	
	result= z--;
	
	printf(" z-- donne: %d,\n", result);
return 0;
}


#############################################################################
#############################################################################


﻿//06L03 p85


#include <stdio.h>

main(){
// resultat de produit par des expressions relationnelles


int x,y;
double z;
	x=7;                         // <  >
	y=25;
	z=24.46;



	printf("comme x= %d, y=%d et z=%d ,\n", x,y);

	printf("x >= y produit %d,\n", x >= y);

	printf("x==y   produit %d,\n",x==y);

	printf("x < z  produit %d,\n", x < z);

	printf("y > z  produit %d,\n", y > z);

	printf("x!=y-18 produit %d,\n", x!=y-18);

	printf("x+y !=z produit %d,\n", x+y !=z);
return 0;
}

/*
; ./exe
comme x= 7, y=25 et z=-1889785610 ,
x >= y produit 0,
x==y   produit 0,
x < z  produit 1,
y > z  produit 1,
x!=y-18 produit 0,
x+y !=z produit 1,
NB : s'agit de comparer les valeur
0 signifie que : c'est faux
1 signifie que : c'est vrai

*/


#############################################################################
#############################################################################


﻿//06L03 p87


#include <stdio.h>

main(){
// operateurs de conversions


int x,y;
	x=7;
	y=5;


	printf("comme x %d et y=%d,\n", x,y);


	printf("x/y donne %d \n", x/y);

	printf("(float) x/y donne %f \n", (float) x/y);
return 0;
}

#############################################################################
#############################################################################

/*07l01 page92

syntaxe de cour:  instruction for

#include <stdio.h>
for( expression1; expression2 ; expression3 ){
	instruction1; 
	instruction2;
    .
	.
	}
*/




#include <stdio.h>
// conversion en hexadecimal en utilisant la boucle for
main(){

    int i;

		printf("affichage des nombres en Hexadecimal majuscule, en hexadecimal miniscule puis en decimal\n");
		printf("Hex maj  Hex min decimal\n");

	for ( i = 0; i < 16; i++){
		printf("% X      % x     % d \n", i,i,i);
							}
	return 0;
		}




#############################################################################
#############################################################################
﻿//07l02 page95

#include <stdio.h>
// conversion en hexadecimal en utilisant la boucle for
main(){

    int i, j;


	for ( i = 0, j=8;  i < 8 ; j>16, i++, j-- ){

            

		printf("% d + % d=% d\n", i,j, i+j);
							}
	return 0;
		}





#############################################################################
#############################################################################

﻿/*07l03 page66

syntaxe de cour: creer une boucle infinie avec instruction for
#include <stdio.h>
for( ; ; ){
	// bloc d'instruction
}
*/




#include <stdio.h>
// instruction for avec expression multiples
main(){

    int i, j, result;


	for ( i = 0, j=1;  i < 8 ; i++, j++ ){
           
		printf("% d + % d=\n", i,j, j-i);
							}
	return 0;
		}





#############################################################################
#############################################################################


﻿/*07L04.....page 98

syntaxe de cours de cours: boucle for conditionnelle()

*/


#include <stdio.h>

main(){

	int c;
	printf(" entre un caractere\n(x pour quitter) \n" );
	for( c=' '; c != 'x';){
		
		c= getc ( stdin);
		putchar (c);
	}

	printf("\n vous venez de quitter la boucle for. A bientot!\n");
	
	return 0;
	}



#############################################################################
#############################################################################


﻿/*07L05.....page 100

#include <stdio.h>
syntaxe de cours de cours: boucle whilee()
for( expression ){
	instruction1;
	instruction2;
    .
	.
}
*/



#include <stdio.h>

main(){

	int c;
	c=' ';

	printf(" entrez un caractere\n(x pour quitter) \n" );
	while( c != 'x'){

		c= getc ( stdin);
		putchar (c);
	}

	printf("\n vous venez de quitter la boucle while. A bientot!\n");


	return 0;
	}



#############################################################################
#############################################################################

﻿/*07L04.....page 102

#include <stdio.h>
syntaxe de cours de cours: boucle whilee()
do( expression ){
	instruction1;
	instruction2;
    .
	.
	}while(expression );
*/



#include <stdio.h>

main(){

	int i;
	i=65;
	do{

	printf(" la valeur numerique de % c est % c \n", i,i);

	i++;
	}while( i<72);

	return 0;
	}



#############################################################################
#############################################################################

﻿/*07L07.....page 103

#include <stdio.h>
syntaxe de cours de cours: boucle imbiquées()
do( expression ){
	instruction1;
	instruction2;
    .
	.
	}while(expression );
*/



#include <stdio.h>

main(){

	int i, j;
	for (i=1; i<=3; i++){ // boucle externe
	printf(" iteration %d de la boucle externe.\n",i); 
	for (j=1; j<=4; j++			// boucle interne
	printf(" iteration %d de la boucle interne.\n",j);
	printf(" fin iteration %d de la boucle externe.\n",i);}

	return 0;
	}




#############################################################################
#############################################################################



/*08l01 page110

syntaxe de cour:  utilisation de l'operateur sizeof

#include <stdio.h>
sizeof (expression);
ex: var= sizeof (int);
	.
	}
*/


#include <stdio.h>

main(){

	char ch;
	int int_num;
	float flt_num;
	double dbl_num;

	printf(" taille de char: %d octets\n", sizeof (char));
	printf(" taille de ch: %d octets\n", sizeof ch);

	printf(" taille de int: %d octets\n", sizeof (int));
	printf(" taille de int_num: %d octets\n", sizeof int_num);

	printf(" taille de float: %d octets\n", sizeof (float));
	printf(" taille de flt_num: %d octets\n", sizeof flt_num);

	printf(" taille de double: %d octets\n", sizeof (double));
	printf(" taille de dbl_num: %d octets\n", sizeof dbl_num);


	return 0;
	}




#############################################################################
#############################################################################

/*08L02 page113

syntaxe de cour:  utilisation de l'operateur ET (&&)

#include <stdio.h>
exp1 && exp2;
exp1 et exp2 correspondant aux deux expresion evaluées;
	.
	}
*/


#include <stdio.h>

main(){

	
	int num;
	
	num=0;
	printf("valeur renvoyer par l'oprateur ET: %d\n",(num%2==0) && ( num%3==0));
	
	num=2;
	printf("valeur renvoyer par l'oprateur ET: %d\n",(num%2==0) && ( num%3==0));

	num=3;
	printf("valeur renvoyer par l'oprateur ET: %d\n",(num%2==0) && ( num%3==0));
	
	num=6;
	printf("valeur renvoyer par l'oprateur ET: %d\n",(num%2==0) && ( num%3==0));

	return 0;
	}




#############################################################################
#############################################################################


/*08L03 page115

syntaxe de cour:  utilisation de l'operateur ET (&&)

#include <stdio.h>
exp1¦¦ exp2;
exp1 et exp2 correspondent aux expresions evaluées;
	.
	}
*/


#include <stdio.h>

main(){

	
	int num;
	
	printf("entrez un chiffre divisible par 2 et 3\n");
	
	for (num=1; (num%2 !=0) || (num%3 !=0) ;)
	num= getchar () -48;
	printf("resultat: %d\n", num);

	return 0;
	}



#############################################################################
#############################################################################

/*08L04 page117

syntaxe de cour:  utilisation de l'operateur NON (!)

#include <stdio.h>
	!expresion;
expresion correspond a l'expresion traitée par l'operateur de negation;
	.
	}
*/


#include <stdio.h>
	
main(){

	
	int num;
	
	num=7;
	printf("num=7\n");
	
	printf("!(num<7) renvoie %d\n",!(num<7));

	printf("!(num>7) renvoie %d\n",!(num>7));
	
	printf("!(num==7) renvoie %d\n",!(num==7));

	return 0;
	}



#############################################################################
#############################################################################
/*08L05 page121

syntaxe de cour:  realisation d'un operateur bit a bit

#include <stdio.h>
	il se presente sur cette forme:
	X & Y
	X ¦ Y
	X ˆ Y
	  ~ X
	  X et Y sont des operandes.
	}
*/


#include <stdio.h>

main(){

	
	int x,y,z;
	x=4321;
	y=5678;
	
	printf("		x= %u ou 0X%04X\n", x, x);
	printf("		y= %u ou 0X%04X\n", y, y);
	
	z= x & y;
	printf("x & y renvoie		%6u ou 0X%04X\n", z, z);
	z= x|y;
	printf("x|y renvoie %6u ou 0X%04X\n",z,z);
	z= x^y;
	printf("x^y renvoie %6u ou 0X%04X\n", z,z);
	printf("~x renvoie %6u ou 0X%04X\n", ~ x, ~x);

	return 0;
	}


#############################################################################
#############################################################################

/*08L06 page123

syntaxe de cour:  operateur de decalage
	il se presente sur cette forme:
	X >> Y decales les bits d'un operande vers la droite 
	X <<Y
	
	}
*/


#include <stdio.h>

main(){


	int x,y,z;
	x=255;
	y=5;

	printf(" comme x= %4d ou 0X%04X\n", x, x);
	printf(" comme y= %4d ou 0X%04X\n", y, y);

	z= x >> y;
	printf("x>> y renvoie %6d ou 0X%04X\n", z, z);
	z = x << y;
	printf(" x << y renvoie %6u ou 0X%04X\n",z,z);            // pas d'espace dans  les format 0X%04X\n 

	return 0;
}




#############################################################################
#############################################################################


/*008L07 page124

syntaxe de cour:  operateur conditionnel
	il s'agit du seul operateur en C qui traite trois (03) operandes a la fois:
	X ? Y : z
		x contient la condition test y etz resentant la valeur finale de l'expression.
		selon que x renvoie une valeur differente de zero ou une valeur nulle. y ou z est
		choisie comme resultat.

*/


#include <stdio.h>

main(){


	int x;
	x = sizeof(int);

printf(" %s\n",
(x == 2) ? " taille de int est egal a 2 octets." :" taille de int differente de 2 octets");

	printf(" valeur maximal de int: %d\n",	(x != 2) ?  ~ (1<< x * 8-1) : ~( 1 << 15) );


	return 0;
}






/*

#include <stdio.h>

main(){


	int x;
	x = sizeof(int);

printf(" (x == 2) ?" );



printf(" taille de int est egal a %s\n octets." , x);




(x == 2) ? " taille de int est egal a 2 octets." :" taille de int differente de 2 octets");

	printf(" valeur maximal de int: %d\n",	(x != 2) ?  ~ (1<< x * 8-1) : ~( 1 << 15) );


	return 0;
}
*/






/*
exe:

taille de int differente de 2 octets
 valeur maximal de int: 2147483647      (suivant le processeur)



*/



#############################################################################
#############################################################################


/*009L01 page131

syntaxe de cour: modificateur signed et unsigned
	ipour les explicationsvoir page130 a 131
*/


#include <stdio.h>

main(){


	signed char ch ;
	int 		x ;
	unsigned int  y;

	ch = 0xFF;
	x=0xFFFF;
	y=0xFFFF;
	printf("valeur decimal de 0xFF signée : %d\n", ch);

	printf(" valeur decimal de 0xFFFF signée : %d\n", x );
	printf(" valeur decimal de 0xFFFF non signée : %u\n", y );
	printf(" valeur hexadecimal de 12345 : %X\n",12345);
	printf(" valeur hexadecimal de -12345 : %X\n", -12345 );


	return 0;
}



#############################################################################
#############################################################################


/*009L02 page134

syntaxe de cour: modification  de la taille de donnees
	modificateur de short et long
*/


#include <stdio.h>

main(){


	
	printf("taille de short int  : %d\n", sizeof (short int) );
	printf("taille de long  int: %d\n", sizeof (long int) );
	printf("taille de float : %d\n", sizeof (float) );
	printf("taille de double : %d\n", sizeof (double) );
	printf("taille de long double : %d\n", sizeof (long double) );

	return 0;
}



#############################################################################
#############################################################################

/*009L04 page137

syntaxe de cour: fonction mathematique
	calculer les valeurs trigonometrique: sin() cos() tangente()
*/


#include <stdio.h>
#include <math.h>
main(){

	double x;
	x = 45.0; 				// 45 degres
	x *= 3.141593 / 180.0 ; // convertir en radians 
	
	printf("sinus de 45 : %f\n", sin(x) );
	printf("cosinus de 45 : %f\n", cos(x) );
	printf("tangente de 45 : %f\n", tan(x) );
	
	return 0;
}




#############################################################################
#############################################################################

/*009L05 page139

syntaxe de cour: fonction mathematique
	fonction pow() et sqrt ()
	#include <math.h>
	double pow ( double x, double y)
	.
	.
	sqrt ( double x)
*/


#include <stdio.h>
#include <math.h>
main(){

	double x, y, z;
	x = 64.0; 	
	y=3.0; 
	z=0.5;
	
	printf(" pow ( 64.0 , 3.0 ) donne %7.0f\n", pow( x, y) );
	printf(" sqrt ( 64.0 ) donne %2.0f\n", sqrt( x) );
	printf(" pow ( 64.0 , 0.5 ) donne %2.0f\n", pow( x, z) );
	
	return 0;
}

#############################################################################
#############################################################################

/*10L01 page145

syntaxe de cour: instructions conditionnelles 
	instructions conditionnelles if
	if(expression)
	instruction1;
	instruction2;
	.
	.
	
*/


#include <stdio.h>
main(){

	int i;
	
	printf(" entier de 0 a 100 divisible\n");
	printf(" par 2 et 3:\n");
	
	for(i=0; i <= 100; i++ )
	if ((i % 2 == 0)&& (i % 3 == 0))
	
	printf ("	% d \n", i);	
	return 0;
}




#############################################################################
#############################################################################



/*10L02 page146

syntaxe de cour: instructions conditionnelles
	instructions conditionnelles if-else
	if(expression){
	instruction1;
	instruction2;
	.
	.
	} else {
	instruction_A;
	instruction1_B;
			}
*/


#include <stdio.h>
main(){

	int i;

	printf(" nombre pair 	nombre impair\n");

	for(i=0; i <= 10; i++ )
	if (i%2 == 0)
	printf ("%d\n", i);
    else
	printf ("%14d\n", i);
	return 0;
}


#############################################################################
#############################################################################

/*10L02 page146

syntaxe de cour: instructions conditionnelles
	instructions conditionnelles if-else
	if(expression){
	instruction1;
	instruction2;
	.
	.
	} else {
	instruction_A;
	instruction1_B;
			}
*/


#include <stdio.h>
main(){

	int i;

	printf(" nombre pair 	nombre impair\n");

	for(i=0; i <= 10; i++ )
	if (i%2 == 0)
	printf ("%d\n", i);
    else
	printf ("%14d\n", i);
	return 0;
}







#############################################################################
#############################################################################

/*10L03 page148

syntaxe de cour: instructions conditionnelles
	instructions conditionnelles if imbriquées
*/


#include <stdio.h>
main(){

	int i;
	for(i= -5; i <= 5; i++ )
	if (i > 0)
    if (i%2 == 0)


	printf(" %d est un  nombre pair\n", i);
		else
		printf(" %d est un  nombre impair\n", i);

		else if (i == 0 )
		printf(" valeur nul\n", i);
	else
	printf ("nombre negatif:	% d \n", i);
	return 0;
}






#############################################################################
#############################################################################

/*10L04 page150

syntaxe de cour: instructions conditionnelles 
	instructions conditionnelles switch
	switch expression{
	case expression1:
		instructions1;
		case expression2:
		instructions2;
		.
		.
		default:
		instruction-par-defaut;
	}
*/


#include <stdio.h>
main(){

	int day;
	
	printf("tapez un numero de jour \n");
	printf("compris entre 1 et 3\n");
	
	day = getchar();
	switch (day){
	case'1':
	printf(" jour 1\n ");
		case'2':
	printf(" jour 2\n ");
			case'3':
	printf(" jour 3\n");

	default:
		;
		}
	return 0;
}






#############################################################################
#############################################################################
/*10L05 page150

syntaxe de cour: instructions conditionnelles
	instructions conditionnelles break




*/


#include <stdio.h>
main(){

	int day;

	printf("tapez un numero de jour \n");
	printf("compris entre 1 et 7\n");

	day = getchar();
	switch (day){
	case'1':
	printf(" jour: dimanche  \n ");
	break;

	case'2':
	printf(" jour: lundi \n ");
	break;

	case'3':
	printf(" jour: mardi  \n ");
	break;

	case'4':
	printf(" jour: mercredi \n ");
	break;

	case'5':
	printf(" jour: jeudi  \n ");
	break;

	case'6':
	printf(" jour: vendredi \n ");
	break;

	case'7':
	printf(" jour: samedi  \n ");
	break;

	default:

	printf(" erreur: le nombre se situe hors intervalle\n ");
	break;
	}
	return 0;
}


#############################################################################
#############################################################################


/*10L06 page153

syntaxe de cour: instructions conditionnelles 
	sortie d'une boucle infinie 
	for (; ;){
		instructions1;
		instructions2;
		.
		.
		}
		while (1){
		instructions1;
		instructions2;		
	}
*/


#include <stdio.h>
main(){

	int c;
	printf("tapez un caractere \n ( x pour quiter )\n");
	while (1){
	c = getc (stdin);
	if ( c == 'x')
	break;
	}
	printf("vous venez de quitter une boucle while infinie. a bientot! \n");
	
	return 0;
}



#############################################################################
#############################################################################


/*10L07 page155

syntaxe de cour: instructions conditionnelles
	instruction continue

*/


#include <stdio.h>
main(){

	int i, sum;
	
	sum=0;
	
	for (i=1; i<8; i++){
	if (( i == 3)|| (i == 5))
	continue;
	
	sum += i;
	}
	printf("somme de 1,2,3,4,5,6 et 7: %d\n",sum);

	return 0;
}



#############################################################################
#############################################################################
/* 11L01.c.....p163

*/ 


#include <stdio.h>

main(){
	 char   c;
	 int    x;
	 float  y;
 	 printf("c:adresse=0x%p; contenu=%c \n",&c, c);
 	 printf("x:adresse=0x%p; contenu=%d \n",&x, x);
 	 printf("y:adresse=0x%p; contenu=%5.2\n",&y, y);

	 c='A';
	 x=7;
	 y=123.45;
	 printf("c: adresse=0x%p; contenu=%c\n",&c, c);
	 printf("x: adresse=0x%p; contenu=%d\n",&x, x);
	 printf("y: adresse=0x%p; contenu=%5.2f\n",&y, y);

	return 0;
}





/*

c:adresse=0x0x7fff4a865e4f; contenu= 
x:adresse=0x0x7fff4a865e48; contenu=0 
y:adresse=0x0x7fff4a865e44; contenu=%5.2
c: adresse=0x0x7fff4a865e4f; contenu=A
x: adresse=0x0x7fff4a865e48; contenu=7
y: adresse=0x0x7fff4a865e44; contenu=123.45

*/


#############################################################################
#############################################################################

/* 11L02.c.....p165
char  *ptr_c; 	// pointeur vers un caractere 
int   *ptr_int; // pointeur vers un entier 
float *ptr_flt; // pointeur vers un nombre decimal

*/ 


#include <stdio.h>

main(){
	char   c, *ptr_c;
	int    x, *ptr_x; 
	 float  y, *ptr_y;
	 
	 c='A';
	 x=7;
	 y= 123.45;
	 
 	 printf("c: adresse=0x%p; contenu=%c \n",&c, c);
 	 printf("x: adresse=0x%p; contenu=%d \n",&x, x);
 	 printf("y: adresse=0x%p; contenu=%5.2f\n",&y, y);
	 
	 ptr_c=&c;
	 printf("ptr_c: adresse=0x%p, contenu=0x%p\n", &ptr_c, ptr_c);
	 printf("*ptr_c => %c\n", *ptr_c);
	 
	 ptr_x = &x;
	 printf("ptr_x: adresse= 0x%p, contenu= 0x%p\n", &ptr_x, ptr_x);
	 printf("*ptr_x => %d\n", *ptr_x);
	 
	 ptr_y=&y;
	 printf("ptr_y: adresse= 0x%p, contenu= 0x%p\n", &ptr_y, ptr_y);
	 printf("*ptr_y => %5.2f, %p\n", *ptr_y);


	 
	return 0;
}


/*
gcc -o  exe   11L02.c ; ./exe


*/



/*co



c: adresse=0x0x7ffd47faebaf; contenu=A 
x: adresse=0x0x7ffd47faeb9c; contenu=7 
y: adresse=0x0x7ffd47faeb8c; contenu=123.45
ptr_c: adresse=0x0x7ffd47faeba0, contenu=0x0x7ffd47faebaf
*ptr_c => A
ptr_x: adresse= 0x0x7ffd47faeb90, contenu= 0x0x7ffd47faeb9c
*ptr_x => 7
ptr_y: adresse= 0x0x7ffd47faeb80, contenu= 0x0x7ffd47faeb8c
*ptr_y => 123.45, 0x7fffffc4


*/










#############################################################################
#############################################################################

/* 11L03.c.....p168
syntaxe de cours: mises a jour de variable par pointeur
modification de valeurs par pointeurs 

*/ 


#include <stdio.h>

main(){
	 char   c, *ptr_c;
	 
	 
	 c='A';
	 
	 
 	 printf("c: adresse=0x%p; contenu=%c \n",&c, c);
	 
	 ptr_c = &c ;
	 printf("ptr_c: adresse=0x%p, contenu=0x%p\n", &ptr_c, ptr_c);
	 printf("*ptr_c => %c\n", *ptr_c);
	 
	 *ptr_c = 'B' ;
	 printf("ptr_c: adresse=0x%p, contenu=0x%p\n", &ptr_c, ptr_c);
	 printf("*ptr_c => %c\n", *ptr_c);
	 printf("c: adresse=0x%p, contenu=0x%p\n", &c, c);

	return 0;
}

/*

NB: >  fait référence

--------------------------------
L'opérateur d'indirection *

l'asterixque    *  porte le nom d'operateurd'indirection( ou d'adressage indirecte) lorsqu'il est utilisé comme comme operateur unaire.

	Il est possible de référencer la valeur d'une variable  en combinant l'operateur * et 'l'operande ( l'élément) contenant  l'adresse de la variable  

Ainsi: 

- avec :  char   x, *ptr_x;
 
l' expresion *ptr_x   >  la valeur de la variable x affectée à   au pointeur ptr_x



Ainsi, 

si x  = 1234, 

on peut  

	- affecter la valeur gauche (adresse)  de x à ptr_x
sous forme : ptr_x = &x


	Donc  l'expression *ptr_x   >   1234  correspondant à la valeur droite (contenu)  de x 
	
	

RESUME:

					x=1234
valeur gauche (adresse de x)				valeur droite (contenu)

	ptr_x = &x			|		*ptr_x   >   1234
	
	

NB:  Pour clarifier la terminologie


   *ptr_x  est une  expresion d'indirection de pointeur : 

        et ptr_x est le pointeur  ( la variable pointeur). 


*/




#############################################################################
#############################################################################

/* 11L03.c.....p168
syntaxe de cours: mises a jour de variable par pointeur
modification de valeurs par pointeurs 

*/ 


#include <stdio.h>

main(){
	 char   c, *ptr_c;
	 
	 
	 c='A';
	 
	 
 	 printf("c: adresse=0x%p; contenu=%c \n",&c, c);
	 
	 ptr_c = &c ;
	 printf("ptr_c: adresse=0x%p, contenu=0x%p\n", &ptr_c, ptr_c);
	 printf("*ptr_c => %c\n", *ptr_c);
	 
	 *ptr_c = 'B' ;
	 printf("ptr_c: adresse=0x%p, contenu=0x%p\n", &ptr_c, ptr_c);
	 printf("*ptr_c => %c\n", *ptr_c);
	 printf("c: adresse=0x%p, contenu=0x%p\n", &c, c);
	 
	 
	return 0;
}




/*co



$ gcc -o  exe   11L03.c ; ./exe
c: adresse=0x0x7ffda2d4c6cf; contenu=A 
ptr_c: adresse=0x0x7ffda2d4c6c0, contenu=0x0x7ffda2d4c6cf
*ptr_c => A
ptr_c: adresse=0x0x7ffda2d4c6c0, contenu=0x0x7ffda2d4c6cf
*ptr_c => B
c: adresse=0x0x7ffda2d4c6cf, contenu=0x0x42

*/
#############################################################################
#############################################################################

/* 11L04.c.....p170
syntaxe de cours: pointeurs multiples
pointeurs multiples
*/


#include <stdio.h>

main(){

	 int    x;
	 int   *ptr_1,*ptr_2,*ptr_3;


	 x=1234;


 	 printf("x: adresse = 0x%p; contenu = %d\n", &x, x);

	 ptr_1 = &x ;
	 printf("ptr_1: adresse = 0x%p, contenu = 0x%p\n", &ptr_1, ptr_1);
	 printf("*ptr_1 => %d\n", *ptr_1);

	 ptr_2 = &x ;
	 printf("ptr_2: adresse=0x%p, contenu=0x%p\n", &ptr_2, ptr_2);
	 printf("*ptr_2 => %d\n", *ptr_2);

	 ptr_3 = ptr_1 ;
	 printf("ptr_3: adresse = 0x%p, contenu = 0x%p\n", &ptr_3, ptr_3);
	 printf("*ptr_3 => %d\n", *ptr_3);

	return 0;
}

/*c

$ gcc -o  exe   11L04.c ; ./exe
x: adresse = 0x0x7ffec4a0bb9c; contenu = 1234
ptr_1: adresse = 0x0x7ffec4a0bb90, contenu = 0x0x7ffec4a0bb9c
*ptr_1 => 1234
ptr_2: adresse=0x0x7ffec4a0bb88, contenu=0x0x7ffec4a0bb9c
*ptr_2 => 1234
ptr_3: adresse = 0x0x7ffec4a0bb80, contenu = 0x0x7ffec4a0bb9c
*ptr_3 => 1234


*/




#############################################################################
#############################################################################



#############################################################################
#############################################################################




/*
long strtol ( const char *start , char ** end , int base );


La fonction lit la chaîne de caractères que vous lui envoyez (start) et essaie de la  convertir en long en utilisant la base indiquée (généralement, on travaille en base 10
car on utilise 10 chi_res di_érents de 0 à 9, donc vous mettrez 10). Elle retourne le nombre qu'elle a réussi à lire. Quant au pointeur de pointeur end, la fonction s'en sert
pour renvoyer la position du premier caractère qu'elle a lu et qui n'était pas un nombre.
On ne s'en servira pas, on peut donc lui envoyer NULL pour lui faire comprendre qu'on ne veut rien récupérer.
La chaîne doit commencer par un nombre, tout le reste est ignoré. Elle peut être précédée d'espaces. Quelques exemples d'utilisation pour bien comprendre le principe :

 long i;

 i = strtol ( "148", NULL , 10 ); // i = 148
 i = strtol ( "148.215", NULL , 10 ); // i = 148
i = strtol ( " 148 .215", NULL , 10 ); // i = 148
 i = strtol ( " 148 +34", NULL , 10 ); // i = 148
 i = strtol ( " 148 feuilles mortes ", NULL , 10 ); // i = 148
 i = strtol ( " Il y a 148 feuilles mortes ", NULL , 10 ); // i =
0 ( erreur : la chaîne ne commence pas par un nombre )


*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void viderBuffer();
int lire ( char *chaine , int longueur );
long lireLong();

int main ( int argc , char * argv []){

long age = 0;

printf (" Quel est votre age ? ");


age = lireLong();
printf ("Ah ! Vous avez donc %ld ans !\n\n",age );

return 0;
}


long lireLong(){
char nombreTexte [100] = {0}; // 100 cases devraient suffire

if(lire (nombreTexte , 100)){
// Si lecture du texte ok , convertir le nombre en long et le retourner

return strtol( nombreTexte , NULL , 10);
}

else
{
// Si probl ème de lecture , renvoyer 0
return 0;
}
 
 
}


int lire ( char *chaine , int longueur ){
char * positionEntree = NULL ;

if ( fgets (chaine , longueur , stdin ) != NULL )
{
positionEntree = strchr (chaine ,'\n');
if ( positionEntree != NULL )
{
*positionEntree = '\0';
}
 else
{
viderBuffer ();
}
return 1;
}
else
{
viderBuffer ();
return 0;
}

}


void viderBuffer(){
int c = 0;
while (c != '\n' && c != EOF )
{
c = getchar ();
}
}


/**

$ gcc  -o exe 04L19.c

$ ./exe
 Quel est votre age ?    15mdlos,dod
Ah ! Vous avez donc 15 ans !


Il purifie toute la chaine entrée par l'utilisateur:'  
Condition: le texte doit commencer par un nombre.
l'espace est autorisé. 




*/








#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################



#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################






#############################################################################
#############################################################################


